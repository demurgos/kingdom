<?php

namespace App\Service;

use App\Entity\Storage;
use App\Entity\UnitArmy;
use App\Entity\BuiltBuildings;
use App\Entity\Constructions;
use App\Entity\Resources;
use App\Entity\ConstructionRequirements;
use App\Entity\ConstructionSchemas;
use App\Entity\MaxStorage;
use App\Entity\ResourceConversionSchemas;
use App\Entity\Titles;
use App\Entity\World\WorldMapNode;
use App\Entity\World\CityNode;
use App\Entity\General;
use Doctrine\ORM\EntityManagerInterface;

Class GameDataService 
{
    private $entityManager;

    private $resourceDict;
    private $constructionDict;
    private $titleDict;
    private $constructionSchemaDict;
    private $constructionRequirementDict;
    private $maxStorageDict;
    private $conversionSchemaDict;

    private $isLoaded = false;
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function loadDicts()
    {
        if($this->isLoaded == true)
        {
            return;
        }

        $this->loadResourceDict();
        $this->loadConstructionDict();
        $this->loadTitleDict();
        $this->loadConstructionRequirementDict();
        $this->loadConstructionSchemaDict();
        $this->loadMaxStorageDict();
        $this->loadConversionSchemaDict();

        $this->isLoaded = true;
    }

    //Loads an array (resourceName => resourceObject)
    private function loadResourceDict()
    {
        $resourceDict = array();
        $resources = $this->entityManager->getRepository(Resources::class)->findAll();
        foreach($resources as $resource)
        {
            $resourceDict[$resource->getName()] = $resource;
        }
        $this->resourceDict = $resourceDict;
    }

    public function getResourceDict() : array
    {
        return $this->resourceDict;
    }

    //Loads an array (constructionName => constructionObject)
    private function loadConstructionDict()
    {
        $constructionDict = array();
        $constructions = $this->entityManager->getRepository(Constructions::class)->findAll();
        foreach($constructions as $construction)
        {
            $constructionDict[$construction->getName()] = $construction;
        }
        $this->constructionDict = $constructionDict;
    }

    public function getConstructionDict() : array
    {
        return $this->constructionDict;
    }

    //Loads an array (titleName => titleObject)
    private function loadTitleDict()
    {
        $titleDict = array();
        $titles = $this->entityManager->getRepository(Titles::class)->findAll();
        foreach($titles as $title)
        {
            $titleDict[$title->getName()] = $title;
        }
        $this->titleDict = $titleDict;
    }

    public function getTitleDict() : array
    {
        return $this->titleDict;
    }

    //Loads an array (constructionName => array(level => [constructionsShemasObjects]))
    private function loadConstructionSchemaDict()
    {
        $schemaDict = array();
        $schemas = $this->entityManager->getRepository(ConstructionSchemas::class)->findAll();
        foreach($schemas as $schema)
        {
            if (!array_key_exists($schema->getConstruction()->getName(), $schemaDict))
            {
                $schemaDict[$schema->getConstruction()->getName()] = [1 => [], 2 => [], 3 => [], 4 => [], 5 => []];
            }
            $schemaDict[$schema->getConstruction()->getName()][$schema->getLevel()][$schema->getResource()->getName()] = $schema;   
        }
        $this->constructionSchemaDict = $schemaDict;
    }

    public function getConstructionSchemaDict() : array
    {
        return $this->constructionSchemaDict;
    }

     //Loads an array (constructionName => array(level => constructionRequirementsObject))
    private function loadConstructionRequirementDict()
    {
        $requirementDict = array();
        $requirements = $this->entityManager->getRepository(ConstructionRequirements::class)->findAll();
        foreach($requirements as $requirement)
        {
            if (!array_key_exists($requirement->getConstruction()->getName(), $requirementDict))
            {
                $requirementDict[$requirement->getConstruction()->getName()] = [];
            }
            $requirementDict[$requirement->getConstruction()->getName()][$requirement->getLevel()] = $requirement;
        }
        $this->constructionRequirementDict = $requirementDict;
    }

    public function getConstructionRequirementDict() : array
    {
        return $this->constructionRequirementDict;
    }

    //Loads an array (resourceName => array(level => MaxStorageObject))
    private function loadMaxStorageDict()
    {
        $maxStorageDict = ['wheat' => [], 'wood' => [], 'iron' => [], 'horse' => [], 'lin' => []];
        $maxStorages = $this->entityManager->getRepository(MaxStorage::class)->findAll();
        foreach ($maxStorages as $maxStorage)
        {
            $maxStorageDict[$maxStorage->getResource()->getName()][$maxStorage->getLevel()] = $maxStorage;
        }
        $this->maxStorageDict = $maxStorageDict;
    }

    public function getMaxStorageDict()
    {
        return $this->maxStorageDict;
    }

    //Loads an array (producedResourceName => array(level => [constructionsShemasObjects]))
    private function loadConversionSchemaDict()
    {
        $schemaDict = array();
        $schemas = $this->entityManager->getRepository(ResourceConversionSchemas::class)->findAll();
        foreach($schemas as $schema)
        {
            if (!array_key_exists($schema->getProducedResource()->getName(), $schemaDict))
            {
                $schemaDict[$schema->getProducedResource()->getName()] = ['building' => $schema->getRequiredBuilding()->getName(), 1 => [], 2 => [], 3 => [], 4 => [], 5 => []];
            }
            $schemaDict[$schema->getProducedResource()->getName()][$schema->getRequiredLevel()][$schema->getRequiredResource()->getName()] = $schema;   
        }
        $this->conversionSchemaDict = $schemaDict;
    }

    public function getConversionSchemaDict() : array
    {
        return $this->conversionSchemaDict;
    }

    public function createBuilding(string $buildingName, int $level) : BuiltBuildings
    {
        $builtBuilding = new BuiltBuildings();
        $building = $this->constructionDict[$buildingName];
        $builtBuilding->setBuilding($building);
        $builtBuilding->setLevel($level);
        return $builtBuilding;
    }

    public function createStorage(string $resourceName, int $amount) : Storage
    {
        $storage = new Storage();
        $resource = $this->resourceDict[$resourceName];
        $storage->setResource($resource);
        $storage->setAmount($amount);
        return $storage;
    }

    public function createUnitArmy(string $unitName, int $amount) : UnitArmy
    {
        $army = new UnitArmy();
        $unit = $this->resourceDict[$unitName];
        $army->setResource($unit);
        $army->setAmount($amount);
        return $army;
    }

    public function createNode(string $name, bool $isCity = false): WorldMapNode
    {
        if ($isCity)
        {
            $node = new CityNode();
        }
        else
        {
            $node = new WorldMapNode();
        }
        $node->setName($name);
        return $node;
    }

    public function createGeneral(string $name, WorldMapNode $node): General
    {
        $general = new General();
        $general->setName($name);
        $general->setLocation($node);
        return $general;
    }
}
