<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Lord;
use App\Entity\City;
use App\Entity\World\CityNode;

/**
 * Lord provider aims to :
 * - Generate a new Lord when the player joins a game and assign it to the user
 * - Generate a city to play with
 */
class LordProvider
{
    const CUSTOM_LORD_CHEAT = false;

    private $gameDataService;

    /**
     * LordProvider constructor.
     * @param GameDataService $gameDataService
     */
    public function __construct(GameDataService $gameDataService)
    {
        $this->gameDataService = $gameDataService;
    }

    /**
     * Generates a new Lord and assigns it to the user that requested it!
     * @param $user
     */
    public function GenerateLord($user)
    {
        if (LordProvider::CUSTOM_LORD_CHEAT)
        {
             $lord = $this->createCustomLord($user);
        }
        else
        {
            $lord = $this->createDefaultLord($user);
        }

        $user->setLord($lord);
        $lord->setName($user->getDisplayName());
    }

    /**
     * Creates a Default Lord with its default city
     * @return Lord
     */
    private function createDefaultLord() : Lord
    {
        $this->gameDataService->loadDicts();

        $city = $this->createDefaultCity();
        $lord = new Lord();
        $lord->setCity($city);

        return $lord;
    }

    /**
     * Creates a Lord with a prebuilt list of buildings, some population and armies
     * @return Lord
     */
    private function createCustomLord() : Lord
    {
        $this->gameDataService->loadDicts();

        $lord = new Lord();
        $lord->setAgeMonth(1);
        $lord->setAgeYear(21);
        $lord->setTitle('marquis');
        $lord->setHealth('excellent');
        $lord->setGlory(17);
        $lord->setMaxGlory(20);

        $city = $this->createDefaultCity();
        $city->addBuiltBuilding($this->gameDataService->createBuilding('palace', 4));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('farm', 3));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('attic', 3));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('workshop', 1));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('barracks', 1));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('headquarters', 5));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('militaryAcademy', 2));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('guardTower', 3));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('archery', 3));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('forge', 3));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('cauldron', 3));
        $city->addStorage($this->gameDataService->createStorage('worker', 1));
        $city->addStorage($this->gameDataService->createStorage('merchant', 1));
        $city->addStorage($this->gameDataService->createStorage('lumberjack', 1));
        $city->addStorage($this->gameDataService->createStorage('citizen', 10));
        $city->addStorage($this->gameDataService->createStorage('gold', 1000));
        $city->addStorage($this->gameDataService->createStorage('horse', 50));
        $city->addStorage($this->gameDataService->createStorage('lin', 50));
        $city->addStorage($this->gameDataService->createStorage('iron', 149));
        $city->addStorage($this->gameDataService->createStorage('wood', 1000));
        $city->getCityNode()->getArmyContainer()->addUnitArmy($this->gameDataService->createUnitArmy('soldier', 7));
        $city->getCityNode()->getArmyContainer()->addUnitArmy($this->gameDataService->createUnitArmy('archer', 3));
        $city->getCityNode()->getArmyContainer()->addUnitArmy($this->gameDataService->createUnitArmy('paladin', 10));
        $city->getCityNode()->getArmyContainer()->addUnitArmy($this->gameDataService->createUnitArmy('ballista', 101));
        $city->getCityNode()->getArmyContainer()->addUnitArmy($this->gameDataService->createUnitArmy('knight', 101));
        $lord->setCity($city);
        
        $paris = $this->gameDataService->createNode('Paris');
        $paris->getArmyContainer()->addUnitArmy($this->gameDataService->createUnitArmy('mountedArcher', 17));
        $paris->setTrade(-4);
        $lord->addTerritory($paris);
        
        $general1 = $this->gameDataService->createGeneral('ORM', $city->getCityNode());
        $general1->setReputation(12);
        $general1->getArmyContainer()->addUnitArmy($this->gameDataService->createUnitArmy('soldier', 7));
        $general1->getArmyContainer()->addUnitArmy($this->gameDataService->createUnitArmy('catapult', 12));
        $general1->getArmyContainer()->addUnitArmy($this->gameDataService->createUnitArmy('paladin', 23));
        $lord->addGeneral($general1);
        $lord->addGeneral($this->gameDataService->createGeneral('Symfony', $city->getCityNode()));

        return $lord;
    }

    /**
     * @return City
     */
    private function createDefaultCity() : City
    {
        $city = new City();
        $city->setName('EternalKingdom');
        $city->addStorage($this->gameDataService->createStorage('wheat', 30));
        $city->addStorage($this->gameDataService->createStorage('farmer', 1));
        $city->addStorage($this->gameDataService->createStorage('remainingHammers', 1));

        $cityNode = new CityNode();
        $cityNode->setName($city->getName());
        $city->setCityNode($cityNode);

        return $city;
    }
}
