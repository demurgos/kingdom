<?php

namespace App\Service;

use App\Enum\ConstructionType;
use App\Enum\GameLogType;
use App\Enum\ResourceType;
use App\Model\Alignment;
use App\Entity\Storage;
use App\Entity\BuiltBuildings;
use App\Entity\Lord;
use App\Entity\City;
use App\Entity\Resources;
use App\Entity\Constructions;
use App\Entity\ConstructionRequirements;
use App\Entity\ConstructionSchemas;
use App\Entity\MaxStorage;
use App\Entity\General;
use App\Entity\UnitArmy;
use App\Service\GameDataService;

/**
 * City manager aims to :
 * - Reacts when alignments are made
 * - Manage Resources, Population, Armies and buildings
 */
class CityManager
{
    const MAX_TURN_COUNT = 100;

    // Building Production Bonus per level (0 to 5)
    const BUILDING_LEVEL_MULTIPLIER = [1, 1.3, 1.55, 1.75, 1.9, 2];
    // Alignment length bonus per size (3 to 8)
    const ALIGNMENT_LENGTH_MULTIPLIER = [1, 1.25, 1.5, 2, 3, 4];

    private $city;
    private $maxStorages;

    //Buffers for all calculations
    private $steps;
    private $cumulativeGains;

    private $hasReset = false;

    public $gameDataService;
    private $gameLogMgr;

    /**
     * CityController constructor.
     * @param GameDataService $gameDataService
     * @param GameLogManager $gameLogManager
     */
    public function __construct(GameDataService $gameDataService, GameLogManager $gameLogManager)
    {
        $this->gameDataService = $gameDataService;
        $this->gameDataService->loadDicts();
        $this->gameLogMgr = $gameLogManager;
    }

    /**
     * Increments the specified villager type and removes one citizen
     * @param City $city
     * @param int $jobType
     * @return bool
     */
    public function convertCitizen(City $city, int $jobType) : bool
    {
        $this->city = $city;
        if($this->city->getStorageCount('citizen') <= 0)
        {
            return false;
        }

        $jobName = "";
        switch ($jobType)
        {
            case 0: $jobName = 'farmer';       break;
            case 1: $jobName = 'lumberjack';   break;
            case 2: $jobName = 'worker';       break;
            case 3: $jobName = 'merchant';     break;
            case 4: $jobName = 'recruiter';    break;
            default: break;
        }

        if(!empty($jobName))
        {
            $storage = $this->getStorage($jobName);
            $storage->setAmount($storage->getAmount() + 1);
            $storage = $this->getStorage('citizen');
            $storage->setAmount($storage->getAmount() - 1);

            $this->addAndFlushCityLog("city.citizen.conversion", ["%job%" => $jobName]);

            return true;
        }

        return false;
    }

    /**
     * Triggered once the turn is over, update resources, population, buildings, army...
     * @param City $city
     * @param array $steps
     * @throws \Exception
     */
    public function convertAlignments(City $city, array $steps)
    {
        $this->city = $city;
        $this->steps = $steps;
        $this->hasReset = false;

        $this->city->setRemainingTurns($this->city->getRemainingTurns() - 1);

        $this->convertAlignmentsToGains();

        if ($this->cumulativeGains['replay'] == 0)
        {
            $consumption = $this->consumeFood();
            $goldConsumption = $this->consumeGold(); // TODO Same for gold!

            // Bug: consumption doesn't take in account new citizens!! D:
            $this->collectAllGains();

            if($goldConsumption > 0)
                $this->addAndFlushCityLog("city.consumption.goodAndGold", ["%foodAmount%" => $consumption, "%goldAmount" => $goldConsumption]);
            else
                $this->addAndFlushCityLog("city.consumption.food", ["%amount%" => $consumption, "%icon%" => $this->gameLogMgr->getResourceIcon("wheat")]);

            $this->makeOld();
        }
        else
        {
            $this->collectAllGains();
        }

        $this->gameLogMgr->startLoggingTransaction();

        if ($this->getStorage('wheat')->getAmount() <= 0)
        {
            $this->triggerStarvation();
        }
        if($this->getStorage('gold')->getAmount() <= 0)
        {
            $this->triggerBankrupt();
        }

        $this->gameLogMgr->stopLoggingTransaction();

        if($this->hasReset)
        {
            $this->addAndFlushCityLog("city.gridReseted");
        }
    }

    /**
     * Cumulate all resource modifications from all alignments
     */
    private function convertAlignmentsToGains()
    {
        $this->cumulativeGains = ['wheat' => 0, 'gold' => 0, 'wood' => 0,
                                  'wheatFromHammer' => 0, 'goldFromHammer' => 0,
                                  'hammer' => 0, 'soldier' => 0, 'citizen' => 0, 'replay' => 0];

        for ($step = 0; $step < count($this->steps); $step++)
        {
            // Skip any step that doesn't have alignments (f.i. RESET) !
            if(!isset($this->steps[$step]->alignments))
            {
                $this->hasReset = true;
                continue;
            }

            for ($i = 0; $i < count($this->steps[$step]->alignments); $i++)
            {
                $alignment = $this->steps[$step]->alignments[$i]; // just to read value not modify it!
                $gain = 0;
                switch($alignment->element)
                {
                    case 0:
                        $gain = $this->getWheatProduction($alignment);
                        $this->cumulativeGains['wheat'] += $gain;
                        break;
                    case 1:
                        $gain = $this->getGoldProduction($alignment);
                        $this->cumulativeGains['gold'] += $gain;
                        break;
                    case 2:
                        $gain = $this->GetWoodProduction($alignment);
                        $this->cumulativeGains['wood'] += $gain;
                        break;
                    case 3:
                        if ($this->city->getConstructionProject() !== null)
                        {
                            $gain = $this->getHammerProduction($alignment);
                            $this->cumulativeGains['hammer'] += $gain;
                        }
                        else if ( $this->city->getBuildingLevel('constructionSite') > 0)
                        {
                            $gain = $this->getConstructionSiteProduction($alignment);
                            $this->cumulativeGains['wheatFromHammer'] += $gain;
                            $this->cumulativeGains['goldFromHammer'] += $gain;
                        }
                        else
                        {
                            $this->cumulativeGains['hammer'] = 0;
                        }
                        break;
                    case 4:
                        if($this->city->getBuildingLevel('barracks') == 0 || !$this->city->isRecruiting())
                        {
                            $this->cumulativeGains['soldier'] = 0;
                        }
                        else
                        {
                            $gain = $this->getSoldierProduction($alignment);
                            $this->cumulativeGains['soldier'] += $gain;
                        }
                        break;
                    case 5:
                        $gain = 1;
                        $this->cumulativeGains['citizen']++;
                        break;
                    case 6:
                        $gain = $this->getReplayProduction($alignment);
                        $this->cumulativeGains['replay'] += $gain;
                        break;
                }

                $this->steps[$step]->alignments[$i]->gain = $gain;
            }
        }
    }


    /* Production Formulas */

    private function getWheatProduction(Alignment $alignment) : int
    {
        $jobCount = $this->city->getStorageCount('farmer');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('farm')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];

        return floor(6 * $buildingMultiplier * $alignmentMultiplier * $jobCount);
    }

    private function getGoldProduction(Alignment $alignment) : int
    {
        $jobCount = $this->city->getStorageCount('merchant');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('market')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];

        return floor(max(1, 6 * $buildingMultiplier * $alignmentMultiplier * $jobCount));
    }

    private function getWoodProduction(Alignment $alignment) : int
    {
        $jobCount = $this->city->getStorageCount('lumberjack');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('hut')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];
        return floor(max(1, 5 * $buildingMultiplier * $alignmentMultiplier * $jobCount));
    }

    private function getHammerProduction(Alignment $alignment) : int
    {
        $jobCount = $this->city->getStorageCount('worker');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('workshop')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];
        return floor($buildingMultiplier * $alignmentMultiplier * $jobCount);
    }

    private function getConstructionSiteProduction(Alignment $alignment) : int
    {
        $jobCount = $this->city->getStorageCount('worker');
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength()-3];
        $constructionSiteLevel = $this->city->getBuildingLevel('constructionSite');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('workshop')];
        return floor($alignmentMultiplier * $constructionSiteLevel * $buildingMultiplier * $jobCount);
    }

    private function getSoldierProduction(Alignment $alignment) : int
    {
        if ($this->city->getBuildingLevel('barracks') == 0 || !$this->city->isRecruiting())
            return 0;

        $jobCount = $this->city->getStorageCount('recruiter');
        return floor($jobCount + $alignment->getLength() - 2);
    }

    private function getReplayProduction(Alignment $alignment) : int
    {
        return $alignment->getLength() - 2;
    }

    /**
     * Add $cumulativeGains variable to stock resources
     */
    private function collectAllGains()
    {
        $this->gameLogMgr->startLoggingTransaction();
        $this->calculateMaxStorages($this->city);
        foreach ($this->cumulativeGains as $resource => $gain)
        {
            // TODO Factorize & do a PostResourceChange with the switch
            switch($resource)
            {
                case 'wheat':
                case 'gold':
                case 'wood':
                    $this->collectResource($resource, $gain);
                    break;
                case 'wheatFromHammer':
                    $storage = $this->getStorage('wheat');
                    $storage->setAmount(min($this->maxStorages['wheat'], $storage->getAmount() + $gain));
                    break;
                case 'goldFromHammer':
                    $storage = $this->getStorage('gold');
                    $storage->setAmount(min($this->maxStorages['gold'], $storage->getAmount() + $gain));
                    break;
                case 'hammer':
                    $this->collectHammer($gain);
                    break;
                case 'soldier':
                    $this->collectSoldier($gain);
                    break;
                case 'citizen':
                    $this->collectCitizen($gain);
                    break;
                case 'replay':
                    $this->collectReplay($gain);
                    break;
            }
            $this->gameLogMgr->stopLoggingTransaction();
        }
    }

    /**
     * Calculate true production amount and waste, log it and store
     * @param $resource
     * @param $gain
     */
    private function collectResource($resource, $gain)
    {
        if($gain <= 0)
            return;

        $storage = $this->getStorage($resource);
        $maxPossible = $this->maxStorages[$resource] - $storage->getAmount();

        if ($gain > $maxPossible)
        {
            $production = $maxPossible;
            $waste = $gain - $maxPossible;
        }
        else
        {
            $production = $gain;
            $waste = 0;
        }

        $storage->setAmount(min($this->maxStorages[$resource], $storage->getAmount() + $production));

        if($production > 0)
        {
            $this->addCityLog("city.resource.production", ["%production%" => $this->gameLogMgr->getResourceIconWithAmount($resource, $production)]);
        }
        if($waste > 0)
        {
            $this->addCityLog("city.resource.waste", ["%waste%" => $this->gameLogMgr->getResourceIconWithAmount($resource, $waste)]);
        }
    }

    private function collectHammer($gain)
    {
        if($gain < 0)
            return;

        $storage = $this->getStorage('remainingHammers');
        $storage->setAmount($storage->getAmount() - $gain);

        if ($storage->getAmount() <= 0 && $this->city->getConstructionProject() != null)
        {
            $this->finishConstruction();
        }

        // TODO Find a way to order logs in a better way! Here we are forced to put this logic after to see the construction finished after the hammer progression log
        if($this->city->getConstructionProject() != null && $gain > 0)
        {
            $this->addCityLog("city.hammer.progress", ["%amount%" => $gain, "%icon%" => $this->gameLogMgr->getIcon("small/icon_hammer.gif")]);
        }
        else if ($this->city->getConstructionProject() == null && $gain == 0)
        {
            $this->addCityLog("city.hammer.noConstruction", ["%amount%" => $gain, "%icon%" => $this->gameLogMgr->getIcon("l_cantbuild.png")]);
        }
    }

    private function collectSoldier($gain)
    {
        if($gain < 0)
            return;

        if($gain == 0)
        {
            $this->addCityLog("city.army.noBarracks");
        }
        else
        {
            $unitArmy = $this->getUnitArmy('soldier');
            $unitArmy->setAmount($unitArmy->getAmount() + $gain);
            $this->addCityLog("city.army.recruitment", ["%amount%" => $gain, "%icon%" => $this->gameLogMgr->getIcon("small/u_soldier.gif")]);
        }
    }

    private function collectCitizen($gain)
    {
        if($gain < 0)
            return;

        $storage = $this->getStorage('citizen');
        $storage->setAmount($storage->getAmount() + $gain);
        for($i = 0; $i < $gain; $i++)
        {
            $this->addCityLog("city.citizen.new", ["%cityName%" => $this->city->getName(), "%icon%" => $this->gameLogMgr->getIcon("small/people_pend.gif")]);
        }
    }

    private function collectReplay($gain)
    {
        if($gain <= 0)
            return;

        $this->city->setRemainingTurns(min(100,$this->city->getRemainingTurns() + $gain));
        $this->addCityLog("city.replay", ["%amount%" => $gain, "%icon%" => $this->gameLogMgr->getIcon("small/res_rec.gif")]);
    }

    /**
     * Removes wheat consumption from wheat storage
     * @return int
     */
    private function consumeFood() : int
    {
        $storage = $this->getStorage('wheat');
        $consumption = $this->city->getPopulationCount();
        $storage->setAmount(max(0, $storage->getAmount() - $consumption));

        return $consumption;
    }

    /**
     * Removes gold consumption from gold storage
     */
    private function consumeGold() : int
    {
        //TODO detect whole commerce/army

        return 0;
    }

    /**
     * Grows the lord old by 1 month every 4 turns
     */
    private function makeOld()
    {
        $lord = $this->city->getLord();
        $lord->setAgeMonth($lord->getAgeMonth() + 0.25);
        if ($lord->getAgeMonth() >= 12)
        {
            $lord->setAgeYear($lord->getAgeYear() + 1);
            $lord->setAgeMonth(0);
        }
    }

    /**
     * Kill one random villager, except the last farmer
     * @throws \Exception
     */
    private function triggerStarvation()
    {
        $aliveCitizenTypes = [];
        foreach ($this->city->getStorages() as $storage)
        {
            if ($storage->getResource()->getResourceType() === ResourceType::TYPE_POPULATION && $storage->getAmount() > 0)
            {
                // Ignore last farmer!
                if ($storage->getAmount() === 1 && $storage->getResource()->getName() === "farmer")
                    continue;

                array_push($aliveCitizenTypes, $storage->getResource()->getName());
            }
        }

        if (count($aliveCitizenTypes) === 0)
        {
            return;
        }

        $starvationRand = 0;
        if (count($aliveCitizenTypes) > 1)
        {
            $starvationRand = random_int(0, count($aliveCitizenTypes)-1);
        }

        $lostCitizenType = $aliveCitizenTypes[$starvationRand];
        $storage = $this->getStorage($lostCitizenType);
        $storage->setAmount($storage->getAmount() - 1);

        $storage = $this->getStorage('wheat');
        $storage->setAmount($this->city->getPopulationCount());

        $this->addCityLog("city.citizen.starvation", ["%jobName%" => $lostCitizenType]);
    }

    private function triggerBankrupt()
    {
        //TODO See documentation on Gitlab wiki
    }

    /**
     * Set the construction project to null, add the buildBuilding/Unit to DB
     */
    private function finishConstruction()
    {
        $construction = $this->city->getConstructionProject();
        if($construction == null)
            return;

        if ($construction->getConstructionType() == 'building')
        {
            if ($this->city->getBuildingLevel($construction->getName()) > 0 )
            {
                $building = $this->getBuiltBuilding($construction->getName());
                $building->setLevel($this->city->getProjectLevel());
            }
            else
            {
                $building = new BuiltBuildings();
                $building->setBuilding($construction);
                $building->setLevel($this->city->getProjectLevel());
                $this->city->addBuiltBuilding($building);
            }

            $this->city->setConstructionProject(null);
            $this->city->setProjectLevel(null);
        }
        else if ($construction->getConstructionType() == 'unit')
        {
            $unitArmy = $this->getUnitArmy($construction->getName());
            $unitArmy->setAmount($unitArmy->getAmount()+1);
            $this->city->setConstructionProject(null);
            $this->city->setProjectLevel(null);
        }

        $translationKey = $this->getConstructionActionTranslationKey($construction,"finished");
        // TODO Add the level as parameters here!
        $this->addCityLog($translationKey, ["%name%" => $construction->getName()]);
    }

    /**
     * Returns an array of ['construction' => $construction, 'level' => $level, 'buildable' => $buildable] arrays.
     * @param City $city
     * @return array
     */
    public function getDisplayedProjects(City $city) : array
    {
        $this->city = $city;
        [$constructionDict, $requirementDict, $schemaDict] = [$this->gameDataService->getConstructionDict(), $this->gameDataService->getConstructionRequirementDict(), $this->gameDataService->getConstructionSchemaDict()];
        $displayedProjects = array();
        foreach(array_values($constructionDict) as $construction)
        {
            for($level=5; $level>0; $level--)
            {
                $requirement = $requirementDict[$construction->getName()][$level];
                $noRequiredBuilding = ($requirement->getRequiredConstruction() == null);
                $requiredBuildingLevelOK = ($noRequiredBuilding || $city->getBuildingLevel($requirement->getRequiredConstruction()->getName()) >= $requirement->getRequiredLevel());
                $previousLevelOK = ($level == 1 || $city->getBuildingLevel($construction->getName()) >= $level - 1 || $construction->getConstructionType() == 'unit');
                $alreadyBuilt = $city->getBuildingLevel($construction->getName()) >= $level;
                if ($requiredBuildingLevelOK && $previousLevelOK && !$alreadyBuilt)
                {
                    $buildable = $this->isABuildableConstruction($construction, $level);
                    array_push($displayedProjects, ['construction' => $construction, 'level' => $level, 'buildable' => $buildable]);
                    break;
                }
            }
        }
        return $displayedProjects;
    }

    /**
     * Returns an array of boolean (buildable, 'title' => titleSatisfied, 'resourceName1' => enoughResource1, 'resourceName2' => enoughResource2...)
     * @param Constructions $construction
     * @param int $level
     * @return bool[]
     */
    private function isABuildableConstruction(Constructions $construction, int $level) : array
    {
        $buildable = [0 => true];
        $requirementDict = $this->gameDataService->getConstructionRequirementDict();
        $requiredTitle = $requirementDict[$construction->getName()][$level]->getRequiredTitle();
        $currentTitle = $this->city->getLord()->getTitle();
        if ($requiredTitle != null)
        {
            $buildable['title'] = $currentTitle >= $requiredTitle;
            $buildable[0] &= $buildable['title']; 
        }
        foreach($construction->getOrderedSchemas()[$level] as $schema)
        {
            $resourceName = $schema->getResource()->getName();
            $possessedAmount = $this->getStorage($resourceName)->getAmount();
            $requiredAmount = $construction->getOrderedSchemas()[$level][$resourceName]->getAmount();
            if ($resourceName != 'remainingHammers')
            {
                $buildable[$resourceName] = $possessedAmount >= $requiredAmount;
                $buildable[0] &= $buildable[$resourceName];
            }
        }
        return $buildable;
    }

    /**
     * Consumes the required resources and start the construction project
     * @param City $city
     * @param string $constructionName
     * @param int $level
     * @return bool
     */
    public function startConstruction(City $city, string $constructionName, int $level) : bool
    {
        $this->city = $city;
        if($this->city->getConstructionProject() !== null)
            return false;

        $constructionDict = $this->gameDataService->getConstructionDict();
        $construction = $constructionDict[$constructionName];
        
        // TODO Also verify the title requirement!
        $buildable = $this->isABuildableConstruction($construction, $level);
        if (!in_array(['construction'=>$construction, 'level' => $level, 'buildable' => $buildable], $this->getDisplayedProjects($this->city)))
            return false;
            
        $this->city->setConstructionProject($construction);
        $this->city->setProjectLevel($level);

        foreach($construction->getOrderedSchemas()[$level] as $schema)
        {
            $resourceName = $schema->getResource()->getName();
            $requiredAmount = $construction->getOrderedSchemas()[$level][$resourceName]->getAmount();
            if ($resourceName == 'remainingHammers')
            {
                $storage = $this->getStorage('remainingHammers');
                $storage->setAmount($requiredAmount);
            }
            elseif (in_array($resourceName, ['wheat', 'wood', 'gold', 'lin', 'iron', 'horse']))
            {
                $storage = $this->getStorage($resourceName);
                if($storage->getAmount() < $requiredAmount)
                    return false;

                $storage->setAmount($storage->getAmount() - $requiredAmount);
            }
            else
            {
                return false;
            }
        }

        $translationKey = $this->getConstructionActionTranslationKey($construction,"started");
        $this->addAndFlushCityLog($translationKey, ["%name%" => $construction->getName()]);
        return true;
    }

    /**
     * Give the required resources back, and stop the construction project
     * @param City $city
     * @return bool
     */
    public function cancelConstruction(City $city) : bool
    {
        // TODO Verify that we have something in construction != null maybe?
        $this->city = $city;
        $this->calculateMaxStorages($this->city);
        $construction = $this->city->getConstructionProject();
        if($construction === null)
            return false;

        $level = $this->city->getProjectLevel();
        foreach($construction->getOrderedSchemas()[$level] as $schema)
        {
            $resourceName = $schema->getResource()->getName();
            $requiredAmount = $construction->getOrderedSchemas()[$level][$resourceName]->getAmount();
            if (in_array($resourceName, ['wheat', 'wood', 'gold', 'lin', 'iron', 'horse']))
            {
                $storage = $this->getStorage($resourceName);
                $storage->setAmount(min($this->maxStorages[$resourceName], $storage->getAmount() + $requiredAmount));
            }
            elseif($resourceName =! 'remainingHammers')
            {
                return false;
            }
        }
        $this->city->setConstructionProject(null);

        $translationKey = $this->getConstructionActionTranslationKey($construction,"cancelled");
        $this->addAndFlushCityLog($translationKey, ["%name%" => $construction->getName(), "%icon%" => $this->gameLogMgr->getIcon("l_cantbuild.png")]);

        return true;
    }

    //Get the matching Storage Object if it exists, else create it with amount 0
    public function getStorage(string $resourceName) : Storage
    {
        foreach($this->city->getStorages() as $storage) 
        {
            if($storage->getResource()->getName() == $resourceName)
            {
                return $storage;
            }
        }
        $storage = $this->gameDataService->createStorage($resourceName, 0);
        $this->city->addStorage($storage);
        return $storage;
    }

    /**
     * Get the matching Construction Object if it has been built, else create one with level 0
     * @param string $buildingName
     * @return BuiltBuildings|null
     */
    private function getBuiltBuilding(string $buildingName) : ?BuiltBuildings
    {
        foreach($this->city->getBuiltBuildings() as $building)
        {
            if($building->getBuilding()->getName() == $buildingName)
            {
                return $building;
            }
        }
        $building = $this->gameDataService->createBuilding($buildingName, 0);
        $this->city->addBuiltBuilding($building);
        return $building;
    }

    /**
     * Calculates the current maxAmounts of each resources for the specified city
     * @param City $city
     */
    private function calculateMaxStorages(City $city)
    {
        $maxStorageDict = $this->gameDataService->getMaxStorageDict();
        $this->maxStorages = [
            'wheat' =>  $maxStorageDict['wheat'][$city->getBuildingLevel('attic')]->getMaxAmount(),
            'gold'  =>  9999,
            'wood'  =>  $maxStorageDict['wood'][$city->getBuildingLevel('hut')]->getMaxAmount(),
            'iron'  =>  $maxStorageDict['iron'][$city->getBuildingLevel('forge')]->getMaxAmount(),
            'horse' =>  $maxStorageDict['horse'][$city->getBuildingLevel('stable')]->getMaxAmount(),
            'lin'   =>  $maxStorageDict['lin'][$city->getBuildingLevel('factory')]->getMaxAmount(),
            ];
    }

    public function calculateAndGetMaxStorages($city)
    {
        $this->calculateMaxStorages($city);
        return $this->maxStorages;
    }

    /**
     * Returns an array filtered by resourceType (producedResourceName => [convertable, requiredResourceName => schemas object]) to be displayed in view
     * @param City $city
     * @param string $filter
     * @return array
     */
    public function getDisplayedConversionSchemas(City $city, string $filter) : array
    {
        $this->city = $city;
        $conversionSchemaDict = $this->gameDataService->getConversionSchemaDict(); $resourceDict = $this->gameDataService->getResourceDict();
        $displayedSchemas = [];
        foreach (array_keys($conversionSchemaDict) as $resourceName)
        {
            if ($resourceDict[$resourceName]->getResourceType() == $filter)
            {
                $buildingName = $conversionSchemaDict[$resourceName]['building'];
                for ($level=5; $level>0; $level--)
                {
                    foreach($conversionSchemaDict[$resourceName][$level] as $schema)
                    {
                        if ($this->city->getBuildingLevel($buildingName) == $schema->getRequiredLevel())
                        {
                            if(!in_array($resourceName, array_keys($displayedSchemas)))
                            {
                                $displayedSchemas[$resourceName] = [];
                            }
                            $displayedSchemas[$resourceName][0] = $this->isConvertable($resourceName, 1);
                            $displayedSchemas[$resourceName][$schema->getRequiredResource()->getName()] = $schema;        
                        }     
                    }
                }
            }  
        }
        return $displayedSchemas;
    }

    /**
     * Add the produced resource and remove the required ones
     * @param City $city
     * @param string $producedResourceName
     * @param int $amount
     * @return bool
     */
    public function convertResources(City $city, string $producedResourceName, int $amount) : bool
    {
        $this->city = $city;
        if ($amount > 0 && $this->isConvertable($producedResourceName, $amount))
        {
            $conversionSchemaDict = $this->gameDataService->getConversionSchemaDict();
            $buildingName = $conversionSchemaDict[$producedResourceName]['building'];
            $schemas = $conversionSchemaDict[$producedResourceName][$this->city->getBuildingLevel($buildingName)];

            $resourceCostName = "undefined";
            $resourceCostAmount = 0;
            foreach(array_keys($schemas) as $resourceName)
            {
                if($schemas[$resourceName]->getRequiredResource()->getResourceType() == 'resource')
                {
                    $resourceCostName = $resourceName;
                    $storage = $this->getStorage($resourceName);
                    $resourceCostAmount = $amount * $schemas[$resourceName]->getRequiredAmount();
                    $storage->setAmount($storage->getAmount() - $resourceCostAmount);
                }
                else if($schemas[$resourceName]->getRequiredResource()->getResourceType() == 'unit')
                {
                    $unitArmy = $this->getUnitArmy($resourceName);
                    $resourceCostAmount = $amount * $schemas[$resourceName]->getRequiredAmount();
                    $unitArmy->setAmount($unitArmy->getAmount() - $resourceCostAmount);
                }
            }

            $key = array_keys($schemas)[0];
            if($schemas[$key]->getProducedResource()->getResourceType() == 'resource')
            {
                $storage = $this->getStorage($producedResourceName);
                $producedAmount = $amount * $schemas[$key]->getProducedAmount();
                $storage->setAmount($storage->getAmount() + $producedAmount);

                $this->addAndFlushCityLog("city.resource.conversion", [
                        "%in%" => $this->gameLogMgr->getResourceIconWithAmount($resourceCostName, $resourceCostAmount),
                        "%out%" => $this->gameLogMgr->getResourceIconWithAmount($producedResourceName, $producedAmount)
                    ]);
            }
            else if($schemas[$key]->getProducedResource()->getResourceType() == 'army')
            {
                $unitArmy = $this->getUnitArmy($producedResourceName);
                $producedAmount = $amount * $schemas[$key]->getProducedAmount();
                $unitArmy->setAmount($unitArmy->getAmount() + $producedAmount);

                $this->addAndFlushCityLog("city.army.conversion", [
                    "%soldierAmount%" => $this->gameLogMgr->getUnitIcon("soldier") . ' ' . $resourceCostAmount,
                    "%upgradeInfo%" => $this->gameLogMgr->getUnitIcon($producedResourceName) . ' ' . $producedAmount
                ]);
            }

            return true;
        }
        return false;
    }

    /**
     * Returns true if the player possesses enough required resources
     * @param string $producedResourceName
     * @param int $amount
     * @return bool
     */
    private function isConvertable(string $producedResourceName, int $amount) : bool
    {
        $isConvertable = true;
        $conversionSchemaDict = $this->gameDataService->getConversionSchemaDict();
        $buildingName = $conversionSchemaDict[$producedResourceName]['building'];
        if ($this->city->getBuildingLevel($buildingName) <= 0)
        {
            return false;
        }
        $schemas = $conversionSchemaDict[$producedResourceName][$this->city->getBuildingLevel($buildingName)];
        foreach(array_keys($schemas) as $resourceName)
        {
            if($schemas[$resourceName]->getProducedResource()->getResourceType() == 'resource')
            {
                $isConvertable &= ($this->city->getStorageCount($resourceName) >= $amount*$schemas[$resourceName]->getRequiredAmount());
                $this->calculateMaxStorages($this->city);
                $isConvertable &= ($this->city->getStorageCount($producedResourceName) <= $this->maxStorages[$producedResourceName] - $amount * $schemas[$resourceName]->getProducedAmount());
            } else if ($schemas[$resourceName]->getProducedResource()->getResourceType() == 'army')
            {
                if ($schemas[$resourceName]->getRequiredResource()->getResourceType() == 'resource')
                {
                    $isConvertable &= ($this->city->getStorageCount($resourceName) >= $amount*$schemas[$resourceName]->getRequiredAmount());
                } else if ($schemas[$resourceName]->getRequiredResource()->getResourceType() == 'army')
                {
                    $isConvertable &= ($this->city->getUnitCount($resourceName) >= $amount*$schemas[$resourceName]->getRequiredAmount());
                }
            }
        }
        return $isConvertable;
    }

    /**
     * Remove gold from stock and add general to DB
     * @param City $city
     * @param string $name
     * @return bool
     */
    public function recruitGeneral(City $city, string $name) : bool
    {
        $this->city = $city;
        $generalCount = $city->getLord()->getGeneralCount();
        $goldOK = $city->getStorageCount('gold') >= (50 * pow(2, $generalCount));
        $numberOfGeneralsOK = $generalCount < $city->getBuildingLevel('headquarters');

        if ($numberOfGeneralsOK && $goldOK)
        {
            $storage = $this->getStorage('gold');
            $storage->setAmount($storage->getAmount() - 50*pow(2, count($city->getLord()->getGenerals())));
            $general = new General();
            $general->setName($name);
            $general->setLocation($this->city->getCityNode());
            $this->city->getLord()->addGeneral($general);

            $this->addAndFlushCityLog("city.general.recruitment");

            return true;
        }

        return false;
    }

    /**
     * Switch on/off recruitment
     * @param City $city
     */
    public function toggleRecruitment(City $city)
    {
        $city->setRecruiting(!$city->isRecruiting());
    }

    private function getUnitArmy(string $unitName): UnitArmy
    {
        foreach($this->city->getCityNode()->getArmyContainer()->getUnitArmies() as $army) 
        {
            if($army->getResource()->getName() == $unitName)
            {
                return $army;
            }
        }
        $army = $this->gameDataService->createUnitArmy($unitName, 0);
        $this->city->getCityNode()->getArmyContainer()->addUnitArmy($army);
        return $army;
    }

    /**
     * Returns a translation key with the specified building and the specified action
     * @param Constructions $construction
     * @param string $action
     * @return string
     */
    private function getConstructionActionTranslationKey(Constructions $construction, string $action) : string
    {
        $translationKey = "city.construction.";
        $translationKey .= $construction->getConstructionType() == ConstructionType::TYPE_BUILDING ? "building" : "unit";
        $translationKey .= ("." . $action);

        return $translationKey;
    }

    /**
     * Used between calls
     * @param string $message
     * @param array $params
     */
    private function addCityLog(string $message, array $params = [])
    {
        $this->gameLogMgr->addPendingLog($this->city->getLord(), GameLogType::TYPE_CITY, $message, $params);
    }

    /**
     * Register the specified log and flush it instanenously
     * @param string $message
     * @param array $params
     */
    private function addAndFlushCityLog(string $message, array $params = [])
    {
        $this->gameLogMgr->startLoggingTransaction();
        $this->addCityLog($message, $params);
        $this->gameLogMgr->stopLoggingTransaction();
    }
}
