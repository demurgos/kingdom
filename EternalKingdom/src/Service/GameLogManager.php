<?php

namespace App\Service;

use App\Entity\GameLog;
use App\Entity\Lord;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * GameLogManager aims to collect logs and store them to DB
 */
class GameLogManager
{
    private $entityManager;

    private $pendingLogs;

    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Call that when you want to start collecting game logs
     */
    public function startLoggingTransaction()
    {
        $this->pendingLogs = new ArrayCollection();
    }

    /**
     * @param string $message
     * @param array $params
     * @param Lord $lord
     * @param string $category
     */
    public function addPendingLog(Lord $lord, string $category, string $message, array $params = [])
    {
        $log = new GameLog($lord, $category, $message, $params);
        $this->pendingLogs->add($log);
    }

    /**
     * Call that when you want to flush all collected game logs
     */
    public function stopLoggingTransaction()
    {
        for ($i = $this->pendingLogs->count() - 1; $i >= 0; $i--)
        {
            $this->entityManager->persist($this->pendingLogs->get($i));
        }
        $this->entityManager->flush();
    }

    /**
     * @param string $resource
     * @param int $amount
     * @return string
     */
    public function getResourceIconWithAmount(string $resource, int $amount) : string
    {
        $classToUse = $amount > 0 ? 'sres' : 'sres disable';
        return "<div class='". $classToUse ."'><span class='number'> " . $amount . " " . $this->getResourceIcon($resource) . "</span></div>";
    }

    public function getResourceIcon(string $resource) : string
    {
        return $this->getIcon("small/res_" . $resource . ".gif");
    }

    public function getUnitIcon(string $unit) : string
    {
        return $this->getIcon("small/u_" . $unit . ".gif");
    }

    /**
     * @param string $iconName icon name including image type
     * @return string
     */
    public function getIcon(string $iconName) : string
    {
        return "<img src='/img/icons/" . $iconName . "'>";
    }
}