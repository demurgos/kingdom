<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserService implements UserServiceInterface
{
    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var UserRepository  */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $repository)
    {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    public function save(User $user): User
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function findUserByUserId(string $userId): ?User
    {
        return $this->repository->findOneByUserId($userId);
    }

    public function createUser(string $userId, string $displayName): User
    {
        $user = new User();
        $user
            ->setUserId($userId)
            ->setDisplayName($displayName)
        ;

        $this->save($user);

        return $user;
    }
}