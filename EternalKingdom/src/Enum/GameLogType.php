<?php

namespace App\Enum;

/**
 * Class GameLogType
 * @package App\Enum
 */
class GameLogType
{
    const TYPE_KINGDOM  = "Kingdom";
    const TYPE_CITY     = "City";
    const TYPE_MAP      = "WorldMap";

    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_KINGDOM,
            self::TYPE_CITY,
            self::TYPE_MAP,
        ];
    }
}