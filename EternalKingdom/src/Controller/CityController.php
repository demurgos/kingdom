<?php

// src/Controller/CityController.php
namespace App\Controller;

use App\Entity\GameLog;
use App\Entity\Lord;
use App\Enum\GameLogType;
use App\Model\SwapCandidate;
use App\Model\Tile;
use App\Service\GridManager;
use App\Service\CityManager;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/*
 * Conseil from Nawadoo
 * Côté serveur il "suffit" d'envisager tous les cas de figure (paramètre manquant, paramètre non numérique, etc)
 * Et veiller à ne pas avoir de paramètres contradictoires. Par exemple pour vérifier un stock il est inutile de transmettre le montant en paramètre, puisqu'on va le récupérer côté serveur de toutes façons
 * C'est un peu toujours les mêmes contrôles
 * Si tu veux commencer doucement, tu peux faire un envoi de formulaire classique qui actualise toute la page, à l'ancienne. Et dans un second temps, remplacer ça par de l'ajax
 */

/**
 * CityController will display the City page
 * It permits to access the Match3 to increase resource stocks & population
 * and permits to make new buildings and improve the army.
 * @package App\Controller
 */
class CityController extends AbstractController
{
    //Managers
    private $gridManager;
    private $cityManager;

    /**
     * CityController constructor.
     * @param GridManager $gridManager
     * @param CityManager $cityManager
     */
    public function __construct(GridManager $gridManager, CityManager $cityManager)
    {
        $this->gridManager = $gridManager;
        $this->cityManager = $cityManager;
    }

    /**
     * City index page
     * @Route("/city")
     * @return Response
     */
    public function city(): Response
    {
        return $this->GenerateCommonCityPageResponse( 'City/index.html.twig');
    }

    /**
     * Page that displays the list of constructions schemas.
     * @Route("/city/chooseConstruction")
     * @return Response
     */
    public function chooseConstruction(): Response
    {
        $lord = $this->getCurrentLord();
        $allParameters = [
            'constructionProjects' =>$this->cityManager->getDisplayedProjects($lord->getCity())
        ];
        return $this->GenerateCommonCityPageResponse('City/optionChooseBuilding.html.twig', $allParameters);
    }

     /**
     * Request needs the constructionName !
     * @Route("/city/startConstruction/{name}_{level}")
     * @param string $name
     * @param int $level
     * @return Response
     */
    public function startConstruction(string $name, int $level): Response
    {
        $lord = $this->getCurrentLord();
        if ($this->cityManager->startConstruction($lord->getCity(), $name, $level))
        {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute("app_city_city");
        }

        return $this->redirectToRoute("app_city_chooseconstruction");
    }

    /**
     * @Route("/city/cancelConstruction")
     * @return Response
     */
    public function cancelConstruction(): Response
    {
        $lord = $this->getCurrentLord();
        if ($this->cityManager->cancelConstruction($lord->getCity()))
        {
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->redirectToRoute("app_city_city");
    }

    /**
     * @Route("/city/chooseSoldierUpgrade")
     * @return Response
     */
    public function chooseSoldierUpgrade(): Response
    {
        $lord = $this->getCurrentLord();
        $allParameters = [
            'upgradeSchemas' => $this->cityManager->getDisplayedConversionSchemas($lord->getCity(), 'army')
        ];
        return $this->GenerateCommonCityPageResponse( 'City/optionUpgradeSoldier.html.twig', $allParameters);
    }

    /**
     * Request needs the producedResourceName !
     * @Route("/city/upgradeSoldier/{resourceName}", methods={"POST"}))
     * @param Request $request
     * @param string $resourceName
     * @return Response
     */
    public function upgradeSoldier(Request $request, string $resourceName): Response
    {
        $lord = $this->getCurrentLord();
        $amount = $request->request->get('count');
        if ($this->cityManager->convertResources($lord->getCity(), $resourceName, $amount))
        {
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->redirectToRoute("app_city_choosesoldierupgrade");
    }

    /**
     * Request needs the producedResourceName !
     * @Route("/city/toggleRecruitment"))
     * @return Response
     */
    public function toggleRecruitment(): Response
    {
        $lord = $this->getCurrentLord();
        $this->cityManager->toggleRecruitment($lord->getCity());
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute("app_city_choosesoldierupgrade");
    }

    /**
     * @Route("/city/chooseResourceConversion")
     * @return Response
     */
    public function chooseResourceConversion(): Response
    {
        $lord = $this->getCurrentLord();
        $allParameters = [
            'conversionSchemas' =>$this->cityManager->getDisplayedConversionSchemas($lord->getCity(), 'resource')
        ];
        return $this->GenerateCommonCityPageResponse('City/optionConvertResources.html.twig', $allParameters);
    }

    /**
     * Request needs the producedResourceName!
     * @Route("/city/convertResources/{resourceName}", methods={"POST"}))
     * @param Request $request
     * @param string $resourceName
     * @return Response
     */
    public function convertResources(Request $request, string $resourceName): Response
    {
        $lord = $this->getCurrentLord();
        $amount = $request->request->get('count');

        if ($this->cityManager->convertResources($lord->getCity(), $resourceName, $amount))
        {
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute("app_city_chooseresourceconversion");
    }

    /**
     * @Route("/city/chooseGeneralName")
     * @return Response
     */
    public function chooseGeneralName(): Response
    {
        return $this->GenerateCommonCityPageResponse( 'City/optionRecruitGeneral.html.twig');
    }

    /**
     * Request needs the producedResourceName !
     * @Route("/city/recruitGeneral", methods={"POST"}))
     * @param Request $request
     * @return Response
     */
    public function recruitGeneral(Request $request): Response
    {
        $lord = $this->getCurrentLord();
        $name = $request->request->get('name');

        if ($this->cityManager->recruitGeneral($lord->getCity(), $name))
        {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute("app_map_map");
        }

        return $this->redirectToRoute("app_city_city");
    }

    /**
     * Converts a pending citizen into the specified one
     * Request got the citizen job within a payload
     * It replies with the citizen job that has been created
     *
     * @Route("/city/convertCitizen", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function ConvertCitizen(Request $request): Response
    {
        $response = new Response();
        $statusCode = Response::HTTP_FORBIDDEN;

        $payload = json_decode($request->request->get("json"), true);
        $citizenId = $payload['citizenId'];

        $lord = $this->getCurrentLord();
        if ($this->cityManager->convertCitizen($lord->getCity(), $citizenId))
        {
            $statusCode = Response::HTTP_OK;
            $this->getDoctrine()->getManager()->flush();
            $response->setContent(json_encode($citizenId));
        }

        $response->setStatusCode($statusCode);

        return $response;
    }

    /**
     * Request needs the swap coordinates from client JSON paylood !
     * @Route("/city/swapTiles", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function swapTiles(Request $request): Response
    {
        $response = new Response();
        $statusCode = Response::HTTP_FORBIDDEN;

        $payload = json_decode($request->request->get("json"), true);

        $tile1 = new Tile($payload['r1'], $payload['c1']);
        $tile2 = new Tile($payload['r2'], $payload['c2']);
        $swapCandidate = new SwapCandidate($tile1, $tile2);

        $lord = $this->getCurrentLord();
        if ($this->gridManager->swap($lord->getCityRef(), $swapCandidate))
        {
            $statusCode = Response::HTTP_OK;

            //if the grid has been reset, $steps contains also a string of the new grid
            $steps = $this->gridManager->getChainReactionSteps();

            $this->cityManager->convertAlignments($lord->getCityRef(), $steps);
            $this->getDoctrine()->getManager()->flush();

            $clientPayload = array();
            $clientPayload['steps'] = $steps;
            $clientPayload['resources'] = $this->generateCityInfoAfterSwap($lord);

            $response->setContent(json_encode($clientPayload));
        }

        $response->setStatusCode($statusCode);

        return $response;
    }

    /**
     * Request needs the swap coordinates from client JSON paylood !
     * @Route("/city/fetchLastSwapLogs", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function fetchLastSwapLogs(Request $request)
    {
        $lord = $this->getCurrentLord();

        $logsRepo = $this->getDoctrine()->getRepository("App:GameLog");
        $mostRecentGameLog = $logsRepo->findMostRecentDate($lord, GameLogType::TYPE_CITY);

        $logs = $logsRepo->findRecentGameLogs($lord, GameLogType::TYPE_CITY, $mostRecentGameLog->getTimestamp());

        return new Response($this->render("City/Components/LogsList.html.twig", ["logs" => $logs ])->getContent());
    }

    /**
     * Generates the city info after the swap triggered the full chain reaction
     * @param Lord $lord
     * @return array
     */
    private function generateCityInfoAfterSwap(Lord $lord): array
    {
        $resources = [];
        foreach ($lord->getCity()->getStorages()->getIterator() as $i => $item)
        {
            $resourceInfo = array(
                "name" => $item->getResource()->getName(),
                "amount" => $item->getAmount()
            );
            array_push($resources, $resourceInfo);
        }

        $resourceInfo = array(
            "name" => "turn",
            "amount" => $lord->getCity()->getRemainingTurns(),
        );
        array_push($resources, $resourceInfo);

        $resourceInfo = array(
            "name" => "soldier",
            "amount" => $lord->getCity()->getCityNode()->getArmyContainer()->getUnitCount('soldier'),
        );
        array_push($resources, $resourceInfo);

        return $resources;
    }

    /**
     * Generate a common city page response
     * @param string $view Template to display
     * @param array $extraParameters Extra parameters to load with the template
     * @return Response
     */
    private function GenerateCommonCityPageResponse(string $view, array $extraParameters = []): Response
    {
        $lord = $this->getCurrentLord();
        if($lord == null)
            return $this->redirect($this->generateUrl('homepage'));

        $gameLogRepository = $this->getDoctrine()->getManager()->GetRepository(GameLog::class);
        $cityLogs = $gameLogRepository->findBy(["lord" => $lord, "category" => GameLogType::TYPE_CITY], ['timestamp' => 'DESC']);

        // TODO Only send a short amount of logs! Limit result!

        $this->initCityGrid($lord);
        $commonParameters = [
            'lord' => $lord,
            'maxAmounts' => $this->cityManager->calculateAndGetMaxStorages($lord->getCity()),
            'logs' => $cityLogs,
        ];
        $allParameters = array_merge($commonParameters, $extraParameters);

        return $this->render($view, $allParameters);
    }

    /**
     * Returns the current active Lord
     * @return Lord
     */
    private function getCurrentLord(): ?Lord
    {
        // TODO for debug purpose, we could impersonate another lord here, or use the Switch user feature of Symfony?
        /*
        $lordRepository = $this->getDoctrine()->getManager()->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        */

        return $this->getUser()->getLord();
    }

    /**
     * Initializes a new city's grid & persist it
     * @param Lord $lord
     */
    private function initCityGrid(Lord $lord)
    {
        if (empty($lord->getCity()->getGrid()))
        {
            $grid = $this->gridManager->initGrid($lord->getCity());
            $lord->getCity()->setGrid(GridManager::convertGridToString($grid));
            $this->GetDoctrine()->getManager()->flush();
        }
    }
}