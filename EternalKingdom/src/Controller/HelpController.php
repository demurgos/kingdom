<?php

// src/Controller/HelpController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelpController extends AbstractController
{
    // TODO Create all help pages !!!

    /**
     * @Route("/help")
     */
    public function Help(): Response
    {
        return $this->render('Help/help.html.twig');
    }
}