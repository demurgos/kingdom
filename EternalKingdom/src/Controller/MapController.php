<?php

// src/Controller/MapController.php
namespace App\Controller;

use App\Entity\Lord;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MapController extends AbstractController
{
    // TODO Add an ID here!
    /**
     * @Route("/map")
     */
    public function Map(): Response
    {
        $lord = $this->getUser()->getLord();
        if($lord == null)
            return $this->redirect($this->generateUrl('homepage'));

        $params = [
            "lord" => $lord
        ];

        return $this->render('Map/map.html.twig', $params);
    }

    /**
     * @Route("/map/modify/{id}")
     * @param int $id
     * @return Response
     */
    public function modify($id): Response
    {
        throw $this->createAccessDeniedException('Not implemented');
    }

    /**
     * @Route("/map/ranks/{id}")
     * @param int id worldId
     * @return Response
     */
    public function ranks($id): Response
    {
        // TODO Get all Lords of the World!

        $lordRepository = $this->getDoctrine()->getManager()->GetRepository(Lord::class);
        $lords = $lordRepository->findBy([], ['ageYear' => 'DESC', 'ageMonth' => 'DESC']);

        return $this->render('Map/ranks.html.twig', ["lords" => $lords]);
    }
}