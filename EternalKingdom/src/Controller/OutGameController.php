<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Service\LordProvider;

/**
 * Every actions that doesn't concern in game options!
 */
class OutGameController extends AbstractController
{
    private $lordProvider;

    const CHEAT_ALLOW_FORCE_RESTART = true;

    /**
     * DefaultController constructor.
     * @param LordProvider $lordProvider
     */
    public function __construct(LordProvider $lordProvider)
    {
        $this->lordProvider = $lordProvider;
    }

    /**
     * Displays the World selection
     * @Route("/chooseWorld")
     */
    public function ServerSelection(): Response
    {
        // TODO Load available servers from DB relevant for the current user (difficulty / language preference?)
        // Server Object : ID, name, CustomOwner, players, max, difficulty

        return $this->render('Game/ServerSelection.html.twig');
    }

    /**
     * Displays the World selection
     * @Route("/joinGame/{worldId}")
     */
    public function JoinGame($worldId): Response
    {
        $user = $this->getUser();
        if($user && $user->getLord() === null || OutGameController::CHEAT_ALLOW_FORCE_RESTART)
        {
            dump("TODO We should join the world [" . $worldId ."]");
            $this->lordProvider->generateLord($user);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->forward('App\Controller\DefaultController::Index');
    }

    /**
     * Displays the Death Page
     * @Route("/death")
     */
    public function Death(): Response
    {
        return $this->render('Game/Death.html.twig');
    }
}