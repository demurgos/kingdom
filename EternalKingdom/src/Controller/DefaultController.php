<?php

// src/Controller/DefaultController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * Index redirect to the appropriate homepage (Ex: Index, ServerSelection, Management, Death, ...).
     * @Route("/", name="homepage")
     */
    public function Index(): Response
    {
        $user = $this->getUser();
        $isConnected = $user != null;

        if($isConnected)
        {
            $lord = $user->getLord();
            $hasGameInProgress = $lord !== null;

            // TODO Check if we're redirect to death if we try to access gameplay page (City, map, management, battle, ... (without specific ID))
            if($hasGameInProgress)
            {
                $parameters = [
                    'id' => $lord->getId()
                ];

                if($lord->isAlive())
                    return $this->forward('App\Controller\ManagementController::Management', $parameters);
                else
                    return $this->forward('App\Controller\OutGameController::Death', $parameters);
            }
            else
            {
                return $this->forward('App\Controller\OutGameController::ServerSelection');
            }
        }

        return $this->render('index.html.twig');
    }

    /**
     * Route allowing to change the default locale!
     * @Route("/lang/{_locale}/", name="homepage_locale")
     */
    public function ChangeLanguage(Request $request): Response
    {
        $isConnected = $this->getUser() != null;
        if($isConnected)
        {
            return $this->redirect($request->headers->get('referer'));
        }
        else
        {
            // When not connected, the webpage is not translated since we rely on a session variable.
            return $this->forward("App\Controller\DefaultController::Index");
        }
    }
}