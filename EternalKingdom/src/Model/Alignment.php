<?php

namespace App\Model;

//An alignement contains a list of Tiles of the the same element. A valid alignement has a minimum length of 3
class Alignment
{
	public $tiles;
	public $element;
	public $gain;

	public function __construct($a_tiles, $a_element)
	{
		$this->tiles = $a_tiles;
		$this->element = $a_element;
		$this->gain = 0;
	}

	public static function InvalidAlignment(): Alignment
	{
		return new Alignment([], -1);
	}

	public function getLength(): int
    {
		return count($this->tiles);
	}

	public function isValidAlignment(): bool
	{
		return $this->getLength() >= 3 && $this->element != -1;
	}
}
