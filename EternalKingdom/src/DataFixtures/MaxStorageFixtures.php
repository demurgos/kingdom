<?php

namespace App\DataFixtures;

use App\Entity\MaxStorage;
use App\Service\GameDataService;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;


class MaxStorageFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    private $maxStorages = [
        ['buildingName' => 'attic', 'level' => 0, 'resourceName' => 'wheat', 'maxAmount' => 100], 
        ['buildingName' => 'attic', 'level' => 1, 'resourceName' => 'wheat', 'maxAmount' => 250], 
        ['buildingName' => 'attic', 'level' => 2, 'resourceName' => 'wheat', 'maxAmount' => 600], 
        ['buildingName' => 'attic', 'level' => 3, 'resourceName' => 'wheat', 'maxAmount' => 1500], 
        ['buildingName' => 'attic', 'level' => 4, 'resourceName' => 'wheat', 'maxAmount' => 3500], 
        ['buildingName' => 'attic', 'level' => 5, 'resourceName' => 'wheat', 'maxAmount' => 9999], 
        ['buildingName' => 'hut', 'level' => 0, 'resourceName' => 'wood', 'maxAmount' => 100], 
        ['buildingName' => 'hut', 'level' => 1, 'resourceName' => 'wood', 'maxAmount' => 300], 
        ['buildingName' => 'hut', 'level' => 2, 'resourceName' => 'wood', 'maxAmount' => 500], 
        ['buildingName' => 'hut', 'level' => 3, 'resourceName' => 'wood', 'maxAmount' => 2000], 
        ['buildingName' => 'hut', 'level' => 4, 'resourceName' => 'wood', 'maxAmount' => 5000], 
        ['buildingName' => 'hut', 'level' => 5, 'resourceName' => 'wood', 'maxAmount' => 9999],
        ['buildingName' => 'stable', 'level' => 0, 'resourceName' => 'horse', 'maxAmount' => 10], 
        ['buildingName' => 'stable', 'level' => 1, 'resourceName' => 'horse', 'maxAmount' => 50], 
        ['buildingName' => 'stable', 'level' => 2, 'resourceName' => 'horse', 'maxAmount' => 100], 
        ['buildingName' => 'stable', 'level' => 3, 'resourceName' => 'horse', 'maxAmount' => 150], 
        ['buildingName' => 'stable', 'level' => 4, 'resourceName' => 'horse', 'maxAmount' => 200], 
        ['buildingName' => 'stable', 'level' => 5, 'resourceName' => 'horse', 'maxAmount' => 500],
        ['buildingName' => 'forge', 'level' => 0, 'resourceName' => 'iron', 'maxAmount' => 10],
        ['buildingName' => 'forge', 'level' => 1, 'resourceName' => 'iron', 'maxAmount' => 50], 
        ['buildingName' => 'forge', 'level' => 2, 'resourceName' => 'iron', 'maxAmount' => 100], 
        ['buildingName' => 'forge', 'level' => 3, 'resourceName' => 'iron', 'maxAmount' => 150], 
        ['buildingName' => 'forge', 'level' => 4, 'resourceName' => 'iron', 'maxAmount' => 200], 
        ['buildingName' => 'forge', 'level' => 5, 'resourceName' => 'iron', 'maxAmount' => 500],
        ['buildingName' => 'factory', 'level' => 0, 'resourceName' => 'lin', 'maxAmount' => 10], 
        ['buildingName' => 'factory', 'level' => 1, 'resourceName' => 'lin', 'maxAmount' => 50], 
        ['buildingName' => 'factory', 'level' => 2, 'resourceName' => 'lin', 'maxAmount' => 100], 
        ['buildingName' => 'factory', 'level' => 3, 'resourceName' => 'lin', 'maxAmount' => 150], 
        ['buildingName' => 'factory', 'level' => 4, 'resourceName' => 'lin', 'maxAmount' => 200], 
        ['buildingName' => 'factory', 'level' => 5, 'resourceName' => 'lin', 'maxAmount' => 500],
    ];

    private $manager;
    private $output;
    private $gameDataService;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->output = new ConsoleOutput();
        $this->gameDataService = new GameDataService($this->entityManager);
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;   
        $this->gameDataService->loadDicts();
        [$constructionDict, $resourceDict] = [$this->gameDataService->getConstructionDict(), $this->gameDataService->getResourceDict()];

        foreach ($this->maxStorages as $maxStorageInfo)
        {
            $maxStorage = new MaxStorage();
            $maxStorage->setBuilding($constructionDict[$maxStorageInfo['buildingName']]);
            $maxStorage->setLevel($maxStorageInfo['level']);
            $maxStorage->setResource($resourceDict[$maxStorageInfo['resourceName']]);
            $maxStorage->setMaxAmount($maxStorageInfo['maxAmount']);
            $manager->persist($maxStorage);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [ResourceFixtures::class, ConstructionFixtures::class];
    }

    public static function getGroups(): array
    {
        return ['prod'];
    }
}
