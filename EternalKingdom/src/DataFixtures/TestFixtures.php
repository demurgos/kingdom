<?php

namespace App\DataFixtures;

use App\Entity\Lord;
use App\Entity\City;
use App\Entity\BuiltBuildings;
use App\Entity\Storage;
use App\Entity\Resources;
use App\Entity\Constructions;
use App\Entity\CityNode;
use App\Entity\ArmyContainer;
use App\Enum\TitleType;
use App\Enum\HealthType;
use App\Service\GameDataService;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;

class TestFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    private $output;
    private $gameDataService;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->output = new ConsoleOutput();
        $this->gameDataService = new GameDataService($entityManager);
        $this->gameDataService->loadDicts();
        $this->entityManager = $entityManager;
    }


    public function load(ObjectManager $manager)
    {
        $manager->flush();
    }

    private function CreateDefaultLord() : Lord
    {
        $lord = new Lord();
        $city = $this->createDefaultCity();
        $lord->setCity($city);

        return $lord;
    }

    private function createDefaultCity() : City
    {
        $city = new City();
        $city->setName('EternalKingdom');
        $city->addStorage($this->gameDataService->createStorage('wheat', 30));
        $city->addStorage($this->gameDataService->createStorage('farmer', 1));
        $city->addStorage($this->gameDataService->createStorage('remainingHammers', 1));
        $city->setCityNode($this->gameDataService->createNode($city->getName(), true));

        return $city;
    }

    private function CustomizeLord(String $name) : Lord
    {
        $lord = $this->entityManager->getRepository(Lord::class)->findOneBy(['name' => $name]);
        $lord->setName('Bob');
        $lord->setAgeMonth(1);
        $lord->setAgeYear(21);
        $lord->setTitle('Marquis');
        $lord->setHealth('Excellent');
        $lord->setGlory(17);
        $lord->setMaxGlory(20);
        $city = $this->createDefaultCity();
        $city->addBuiltBuilding($this->gameDataService->createBuilding('palace', 4));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('farm', 3));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('attic', 3));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('workshop', 1));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('barracks', 1));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('headquarters', 5));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('militaryAcademy', 2));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('guardTower', 3));
        $city->addBuiltBuilding($this->gameDataService->createBuilding('factory', 3));
        $city->addStorage($this->gameDataService->createStorage('worker', 1));
        $city->addStorage($this->gameDataService->createStorage('merchant', 1));
        $city->addStorage($this->gameDataService->createStorage('lumberjack', 1));
        $city->addStorage($this->gameDataService->createStorage('citizen', 10));
        $city->addStorage($this->gameDataService->createStorage('gold', 1000));
        $city->addStorage($this->gameDataService->createStorage('horse', 50));
        $city->addStorage($this->gameDataService->createStorage('lin', 50));
        $city->addStorage($this->gameDataService->createStorage('wood', 1000));
        $city->getCityNode()->getArmyContainer()->addArmy($this->gameDataService->createArmy('soldier', 7));
        $city->getCityNode()->getArmyContainer()->addArmy($this->gameDataService->createArmy('archer', 3));
        $city->getCityNode()->getArmyContainer()->addArmy($this->gameDataService->createArmy('paladin', 10));
        $city->getCityNode()->getArmyContainer()->addArmy($this->gameDataService->createArmy('ballista', 101));
        $lord->setCity($city);
        $paris = $this->gameDataService->createNode('Paris');
        $paris->getArmyContainer()->addArmy($this->gameDataService->createArmy('mountedArcher', 17));
        $paris->setTrade(-4);
        $lord->addTerritory($paris);

        return $lord;
    }

    public function getDependencies(): array
    {
        return [ResourceFixtures::class, ConstructionFixtures::class, ConstructionRequirementFixtures::class, ConstructionSchemaFixtures::class,
                MaxStorageFixtures::class, ResourceConversionSchemaFixtures::class];
    }

    public static function getGroups(): array
    {
        return ['dev'];
    }
}
