<?php

namespace App\DataFixtures;

use App\Entity\Resources;
use App\Enum\ResourceType;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;

class ResourceFixtures extends Fixture implements FixtureGroupInterface
{
    public $resources = [
        //Resources
        ['name' => 'wheat', 'type' => ResourceType::TYPE_RESOURCE], 
        ['name' => 'gold', 'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'wood', 'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'lin', 'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'iron', 'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'horse', 'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'remainingHammers', 'type' => ResourceType::TYPE_RESOURCE],
        //Population
        ['name' => 'farmer', 'type' => ResourceType::TYPE_POPULATION], 
        ['name' => 'lumberjack', 'type' => ResourceType::TYPE_POPULATION],
        ['name' => 'merchant', 'type' => ResourceType::TYPE_POPULATION],
        ['name' => 'worker', 'type' => ResourceType::TYPE_POPULATION],
        ['name' => 'recruiter', 'type' => ResourceType::TYPE_POPULATION],
        ['name' => 'citizen', 'type' => ResourceType::TYPE_POPULATION],
        //Army
        ['name' => 'soldier', 'type' => ResourceType::TYPE_ARMY], 
        ['name' => 'pikeman', 'type' => ResourceType::TYPE_ARMY],
        ['name' => 'archer', 'type' => ResourceType::TYPE_ARMY],
        ['name' => 'horseman', 'type' => ResourceType::TYPE_ARMY],
        ['name' => 'knight', 'type' => ResourceType::TYPE_ARMY],
        ['name' => 'mountedArcher', 'type' => ResourceType::TYPE_ARMY],
        ['name' => 'paladin', 'type' => ResourceType::TYPE_ARMY],
        ['name' => 'catapult', 'type' => ResourceType::TYPE_ARMY],
        ['name' => 'ballista', 'type' => ResourceType::TYPE_ARMY]

    ];

    private $entityManager;
    private $output;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    public function load(ObjectManager $manager)
    {
        $this->output = new ConsoleOutput();
        foreach ($this->resources as $resourceData)
        {
            $resource = new Resources();
            $resource->setName($resourceData['name']);
            $resource->setResourceType($resourceData['type']);
            $manager->persist($resource);
        }
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['prod'];
    }
}
