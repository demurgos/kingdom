<?php

namespace App\DataFixtures;

use App\Entity\Constructions;
use App\Entity\Titles;
use App\Enum\TitleType;
use App\Service\GameDataService;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;

class TitleFixtures extends Fixture implements FixtureGroupInterface
{
    public $titles = [
        ['name' => TitleType::TYPE_KNIGHT , 'glory' => 1],
        ['name' => TitleType::TYPE_LORD, 'glory' => 3],
        ['name' => TitleType::TYPE_BARON, 'glory' => 7],
        ['name' => TitleType::TYPE_VISCOUNT, 'glory' => 10],
        ['name' => TitleType::TYPE_COUNT, 'glory' => 15],
        ['name' => TitleType::TYPE_MARQUIS, 'glory' => 20],
        ['name' => TitleType::TYPE_DUKE, 'glory' => 30],
        ['name' => TitleType::TYPE_PRINCE, 'glory' => 50],
        ['name' => TitleType::TYPE_KING, 'glory' => 75],
        ['name' => TitleType::TYPE_EMPEROR, 'glory' => 100],
    ];

    private $manager;
    private $output;


    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->output = new ConsoleOutput();

        foreach ($this->titles as $titleData)
        {
            $title = new Titles();
            $title->setName($titleData['name']);
            $title->setGlory($titleData['glory']);
            $manager->persist($title);
        }
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['prod'];
    }
}
