<?php

namespace App\Entity\World;

use App\Entity\ArmyContainer;
use App\Entity\General;
use App\Entity\Lord;
use App\Repository\WorldMapNodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WorldMapNodeRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"CityNode" = "CityNode", "WorldMapNode" = "WorldMapNode"})
 */
class WorldMapNode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Lord::class, inversedBy="kingdom")
     */
    private $owner;

    /**
     * @ORM\OneToOne(targetEntity=ArmyContainer::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $armyContainer;

    /**
     * @ORM\OneToMany(targetEntity=General::class, mappedBy="location")
     * Generals located on the node
     */
    private $generals;

    /**
     * @ORM\Column(type="integer")
     */
    private $trade = 0;

    public function __construct()
    {
        $this->generals = new ArrayCollection();
        $this->armyContainer = new ArmyContainer();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOwner(): ?Lord
    {
        return $this->owner;
    }

    public function setOwner(?Lord $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getArmyContainer(): ?ArmyContainer
    {
        return $this->armyContainer;
    }

    public function setArmyContainer(ArmyContainer $armyContainer): self
    {
        $this->armyContainer = $armyContainer;

        return $this;
    }

    /**
     * @return Collection|General[]
     */
    public function getGenerals(): Collection
    {
        return $this->generals;
    }

    public function addGeneral(General $general): self
    {
        if (!$this->generals->contains($general)) {
            $this->generals[] = $general;
            $general->setLocation($this);
        }

        return $this;
    }

    public function removeGeneral(General $general): self
    {
        if ($this->generals->removeElement($general)) {
            // set the owning side to null (unless already changed)
            if ($general->getLocation() === $this) {
                $general->setLocation(null);
            }
        }

        return $this;
    }

    public function getTrade(): ?int
    {
        return $this->trade;
    }

    public function setTrade(int $trade): self
    {
        $this->trade = $trade;

        return $this;
    }
}
