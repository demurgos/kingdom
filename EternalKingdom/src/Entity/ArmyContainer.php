<?php

namespace App\Entity;

use App\Repository\ArmyContainerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArmyContainerRepository::class)
 */
class ArmyContainer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=UnitArmy::class, mappedBy="container", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $unitArmies;

    public function __construct()
    {
        $this->unitArmies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|UnitArmy[]
     */
    public function getUnitArmies(): Collection
    {
        return $this->unitArmies;
    }

    public function addUnitArmy(UnitArmy $army): self
    {
        if (!$this->unitArmies->contains($army)) {
            $this->unitArmies[] = $army;
            $army->setContainer($this);
        }

        return $this;
    }

    public function removeUnitArmy(UnitArmy $army): self
    {
        if ($this->unitArmies->removeElement($army)) {
            // set the owning side to null (unless already changed)
            if ($army->getContainer() === $this) {
                $army->setContainer(null);
            }
        }

        return $this;
    }

    public function getUnitCount(String $unitName): int
    {
        foreach($this->unitArmies as $army) 
        {
            if($army->getResource()->getName() !== $unitName) continue;
            return $army->getAmount();
        }
        return 0;
    }

    public function getTotalUnitCount(): int
    {
        $total = 0;
        foreach($this->unitArmies as $army)
        {
            $total += $army->getAmount();
        }
        return $total;
    }
}
