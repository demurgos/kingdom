<?php

namespace App\Entity;

use App\Entity\World\CityNode;
use App\Repository\CityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CityRepository::class)
 */
class City
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity=Lord::class, inversedBy="city", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $lord;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $grid = '';

    /**
     * @ORM\Column(type="smallint")
     */
    private $remainingTurns = 100;

    /**
     * @ORM\Column(type="smallint")
     */
    private $wastedTurns = 0;

    /**
     * @ORM\OneToMany(targetEntity=Storage::class, mappedBy="city", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $storages;

    /**
     * @ORM\OneToMany(targetEntity=BuiltBuildings::class, mappedBy="city", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $builtBuildings;

    /**
     * @ORM\Column(type="boolean")
     */
    private $recruiting = true;

    /**
     * @ORM\ManyToOne(targetEntity=Constructions::class)
     */
    private $constructionProject;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $projectLevel;

    /**
     * @ORM\OneToOne(targetEntity=CityNode::class, mappedBy="city", cascade={"persist", "remove"})
     */
    private $cityNode;

    public function __construct()
    {
        $this->storages = new ArrayCollection();
        $this->builtBuildings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLord(): ?Lord
    {
        return $this->lord;
    }

    public function setLord(Lord $lord): self
    {
        $this->lord = $lord;
        $lord->addTerritory($this->cityNode);

        return $this;
    }

    public function getGrid(): ?string
    {
        return $this->grid;
    }

    public function setGrid(string $grid): self
    {
        $this->grid = $grid;

        return $this;
    }

    public function getRemainingTurns(): ?int
    {
        return $this->remainingTurns;
    }

    public function setRemainingTurns(int $remainingTurns): self
    {
        $this->remainingTurns = $remainingTurns;

        return $this;
    }

    public function getWastedTurns(): ?int
    {
        return $this->wastedTurns;
    }

    public function setWastedTurns(int $wastedTurns): self
    {
        $this->wastedTurns = $wastedTurns;

        return $this;
    }

    /**
     * @return Collection|Storage[]
     */
    public function getStorages(): Collection
    {
        return $this->storages;
    }

    public function addStorage(Storage $storage): self
    {
        if (!$this->storages->contains($storage)) {
            $this->storages[] = $storage;
            $storage->setCity($this);
        }

        return $this;
    }

    public function removeStorage(Storage $storage): self
    {
        if ($this->storages->removeElement($storage)) {
            // set the owning side to null (unless already changed)
            if ($storage->getCity() === $this) {
                $storage->setCity(null);
            }
        }

        return $this;
    }

    public function getStorageCount(string $resourceName) : int //TODO : Maybe optimize ?
    {
        foreach($this->storages as $storage) 
        {
            if($storage->getResource()->getName() !== $resourceName) continue;
            return $storage->getAmount();
        }
        return 0;
    }

    public function getPopulationCount() : int
    {
        $count = 0;
        foreach($this->storages as $storage) 
        {
            if($storage->getResource()->getResourceType() !== 'population') continue;
            $count += $storage->getAmount();
        }
        return $count;
    }

    public function getUnitCount(string $resourceName) : int
    {
        $count = $this->cityNode->getArmyContainer()->getUnitCount($resourceName);
        return $count;
    }

    /**
     * @return Collection|BuiltBuildings[]
     */
    public function getBuiltBuildings(): Collection
    {
        return $this->builtBuildings;
    }

    public function addBuiltBuilding(BuiltBuildings $builtBuilding): self
    {
        if (!$this->builtBuildings->contains($builtBuilding)) {
            $this->builtBuildings[] = $builtBuilding;
            $builtBuilding->setCity($this);
        }

        return $this;
    }

    public function removeBuiltBuilding(BuiltBuildings $builtBuilding): self
    {
        if ($this->builtBuildings->removeElement($builtBuilding)) {
            // set the owning side to null (unless already changed)
            if ($builtBuilding->getCity() === $this) {
                $builtBuilding->setCity(null);
            }
        }

        return $this;
    }

    public function getBuildingLevel(string $buildingName)
    {
        foreach($this->builtBuildings as $builtBuilding) 
        {
            if($builtBuilding->getBuilding()->getName() !== $buildingName) continue;
            return $builtBuilding->getLevel();
        }
        return 0;
    }

    public function isRecruiting(): ?bool
    {
        return $this->recruiting;
    }

    public function setRecruiting(bool $recruiting): self
    {
        $this->recruiting = $recruiting;

        return $this;
    }

    public function getConstructionProject(): ?Constructions
    {
        return $this->constructionProject;
    }

    public function setConstructionProject(?Constructions $constructionProject): self
    {
        $this->constructionProject = $constructionProject;

        return $this;
    }

    public function getProjectLevel(): ?int
    {
        return $this->projectLevel;
    }

    public function setProjectLevel(?int $projectLevel): self
    {
        $this->projectLevel = $projectLevel;

        return $this;
    }

    public function getCityNode(): ?CityNode
    {
        return $this->cityNode;
    }

    public function setCityNode(CityNode $cityNode): self
    {
        // set the owning side of the relation if necessary
        if ($cityNode->getCity() !== $this) {
            $cityNode->setCity($this);
        }

        $this->cityNode = $cityNode;

        return $this;
    }
   

}
