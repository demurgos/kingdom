<?php

namespace App\Entity;

use App\Repository\GameLogRepository;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass=GameLogRepository::class)
 */
class GameLog implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestamp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $log;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $params;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=Lord::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $lord;

    public function __construct(Lord $lord, string $category, string $message, array $params = [])
    {
        $this->timestamp = new \DateTime();
        $this->lord = $lord;
        $this->category = $category;
        $this->log = $message;
        $this->params = $params;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function getMessage(): ?string
    {
        return $this->log;
    }

    public function getLord(): ?Lord
    {
        return $this->lord;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function jsonSerialize(): array
    {
        return array(
            'date' => $this->timestamp,
            'message' => $this->log,
            'params' => $this->params,
        );
    }
}
