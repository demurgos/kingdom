<?php

namespace App\Entity;

use App\Entity\World\WorldMapNode;
use App\Repository\GeneralRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GeneralRepository::class)
 */
class General
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Lord::class, inversedBy="generals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $reputation=1;

    /**
     * @ORM\OneToOne(targetEntity=ArmyContainer::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $armyContainer;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMapNode::class, inversedBy="generals")
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMapNode::class)
     */
    private $destination;

    /**
     * @ORM\Column(type="integer")
     * Percentage of travel
     */
    private $travelProgression = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $fortifying = false;

    public function __construct()
    {
        $this->armyContainer = new ArmyContainer();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?Lord
    {
        return $this->owner;
    }

    public function setOwner(?Lord $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getReputation(): ?int
    {
        return $this->reputation;
    }

    public function setReputation(int $reputation): self
    {
        $this->reputation = $reputation;

        return $this;
    }

    public function getArmyContainer(): ?ArmyContainer
    {
        return $this->armyContainer;
    }

    public function setArmyContainer(ArmyContainer $armyContainer): self
    {
        $this->armyContainer = $armyContainer;

        return $this;
    }

    public function getLocation(): ?WorldMapNode
    {
        return $this->location;
    }

    public function setLocation(?WorldMapNode $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getDestination(): ?WorldMapNode
    {
        return $this->destination;
    }

    public function setDestination(?WorldMapNode $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getTravelProgression(): ?int
    {
        return $this->travelProgression;
    }

    public function setTravelProgression(?int $travelProgression): self
    {
        $this->travelProgression = $travelProgression;

        return $this;
    }

    public function isFortifying(): ?bool
    {
        return $this->fortifying;
    }

    public function setFortifying(bool $fortifying): self
    {
        $this->fortifying = $fortifying;

        return $this;
    }
}
