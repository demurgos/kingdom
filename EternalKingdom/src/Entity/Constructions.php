<?php

namespace App\Entity;

use App\Repository\ConstructionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConstructionsRepository::class)
 */
class Constructions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $constructionType;

    /**
     * @ORM\OneToMany(targetEntity=ConstructionRequirements::class, mappedBy="construction")
     */
    private $constructionRequirements;

    /**
     * @ORM\OneToMany(targetEntity=ConstructionSchemas::class, mappedBy="construction", orphanRemoval=true)
     */
    private $constructionSchemas;


    public function __construct()
    {
        $this->constructionRequirements = new ArrayCollection();
        $this->constructionSchemas = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getConstructionType(): ?string
    {
        return $this->constructionType;
    }

    public function setConstructionType(string $ConstructionType): self
    {
        $this->constructionType = $ConstructionType;

        return $this;
    }

    /**
     * @return Collection|ConstructionRequirements[]
     */
    public function getConstructionRequirements(): Collection
    {
        return $this->constructionRequirements;
    }

    public function addConstructionRequirement(ConstructionRequirements $constructionRequirement): self
    {
        if (!$this->constructionRequirements->contains($constructionRequirement)) {
            $this->constructionRequirements[] = $constructionRequirement;
            $constructionRequirement->setConstruction($this);
        }

        return $this;
    }

    public function removeConstructionRequirement(ConstructionRequirements $constructionRequirement): self
    {
        if ($this->constructionRequirements->removeElement($constructionRequirement)) {
            // set the owning side to null (unless already changed)
            if ($constructionRequirement->getConstruction() === $this) {
                $constructionRequirement->setConstruction(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ConstructionSchemas[]
     */
    public function getConstructionSchemas(): Collection
    {
        return $this->constructionSchemas;
    }

    public function addConstructionSchema(ConstructionSchemas $constructionSchema): self
    {
        if (!$this->constructionSchemas->contains($constructionSchema)) {
            $this->constructionSchemas[] = $constructionSchema;
            $constructionSchema->setConstruction($this);
        }

        return $this;
    }

    public function removeConstructionSchema(ConstructionSchemas $constructionSchema): self
    {
        if ($this->constructionSchemas->removeElement($constructionSchema)) {
            // set the owning side to null (unless already changed)
            if ($constructionSchema->getConstruction() === $this) {
                $constructionSchema->setConstruction(null);
            }
        }

        return $this;
    }

    public function getOrderedSchemas() : array
    {
        $orderedSchemas = [1 => [], 2 => [], 3 => [], 4 => [], 5 => []];
        foreach ($this->getConstructionSchemas() as $schema)
        {
            $orderedSchemas[$schema->getLevel()][$schema->getResource()->getName()] = $schema;
        }
        return $orderedSchemas;
    }

}
