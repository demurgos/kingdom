<?php

namespace App\Repository;

use App\Entity\CityNode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CityNode|null find($id, $lockMode = null, $lockVersion = null)
 * @method CityNode|null findOneBy(array $criteria, array $orderBy = null)
 * @method CityNode[]    findAll()
 * @method CityNode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityNodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CityNode::class);
    }

    // /**
    //  * @return CityNode[] Returns an array of CityNode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CityNode
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
