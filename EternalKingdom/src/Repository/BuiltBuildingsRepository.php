<?php

namespace App\Repository;

use App\Entity\BuiltBuildings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BuiltBuildings|null find($id, $lockMode = null, $lockVersion = null)
 * @method BuiltBuildings|null findOneBy(array $criteria, array $orderBy = null)
 * @method BuiltBuildings[]    findAll()
 * @method BuiltBuildings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuiltBuildingsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BuiltBuildings::class);
    }

    // /**
    //  * @return BuiltBuildings[] Returns an array of BuiltBuildings objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BuiltBuildings
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
