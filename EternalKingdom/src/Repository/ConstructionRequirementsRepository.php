<?php

namespace App\Repository;

use App\Entity\ConstructionRequirements;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConstructionRequirements|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConstructionRequirements|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConstructionRequirements[]    findAll()
 * @method ConstructionRequirements[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConstructionRequirementsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConstructionRequirements::class);
    }

    // /**
    //  * @return ConstructionRequirements[] Returns an array of ConstructionRequirements objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConstructionRequirements
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
