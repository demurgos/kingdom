<?php

namespace App\Repository;

use App\Entity\GameLog;
use App\Entity\Lord;
use App\Enum\GameLogType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GameLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameLog[]    findAll()
 * @method GameLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameLog::class);
    }

    public function findRecentGameLogs(Lord $lord, string $gameLogType, $timestamp)
    {
        $entityManager = $this->getEntityManager();

        $query = $this->createQueryBuilder('e')
            ->where('e.lord = :lord')
            ->andWhere('e.category = :category')
            ->orderBy('e.timestamp', 'DESC')
            ->setParameter('lord', $lord)
            ->setParameter('category', $gameLogType);

        if($timestamp != null)
        {
            $query->andWhere('e.timestamp = :timestamp')
                ->setParameter('timestamp', $timestamp);
        }

        return $query->getQuery()->getResult();
    }

    public function findMostRecentDate(Lord $lord, string $gameLogType)
    {
        $query = $this->createQueryBuilder('e')
            ->where('e.lord = :lord')
            ->andWhere('e.category = :category')
            ->orderBy('e.timestamp', 'DESC')
            ->setParameter('lord', $lord)
            ->setParameter('category', $gameLogType)
            ->getQuery()
            ->setMaxResults(2);

        $result = $query->getResult();
        if(count($result) >= 1)
            return $result[1];

        return null;
    }
}
