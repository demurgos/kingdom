<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220117205919 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Alpha 3';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE army_container_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE game_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE general_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE unit_army_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE world_map_node_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE army_container (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE city_node (id INT NOT NULL, city_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6631D52D8BAC62AF ON city_node (city_id)');
        $this->addSql('CREATE TABLE game_log (id INT NOT NULL, lord_id INT NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, log VARCHAR(255) NOT NULL, params TEXT DEFAULT NULL, category VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_94657B00868E8BB9 ON game_log (lord_id)');
        $this->addSql('COMMENT ON COLUMN game_log.params IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE general (id INT NOT NULL, owner_id INT NOT NULL, army_container_id INT NOT NULL, location_id INT DEFAULT NULL, destination_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, reputation INT NOT NULL, travel_progression INT NOT NULL, fortifying BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CE29364A7E3C61F9 ON general (owner_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CE29364A766D5309 ON general (army_container_id)');
        $this->addSql('CREATE INDEX IDX_CE29364A64D218E ON general (location_id)');
        $this->addSql('CREATE INDEX IDX_CE29364A816C6140 ON general (destination_id)');
        $this->addSql('CREATE TABLE unit_army (id INT NOT NULL, container_id INT NOT NULL, resource_id INT NOT NULL, amount INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_80E7CE6CBC21F742 ON unit_army (container_id)');
        $this->addSql('CREATE INDEX IDX_80E7CE6C89329D25 ON unit_army (resource_id)');
        $this->addSql('CREATE TABLE world_map_node (id INT NOT NULL, owner_id INT DEFAULT NULL, army_container_id INT NOT NULL, name VARCHAR(50) NOT NULL, trade INT NOT NULL, discr VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E3B7CEA97E3C61F9 ON world_map_node (owner_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E3B7CEA9766D5309 ON world_map_node (army_container_id)');
        $this->addSql('ALTER TABLE city_node ADD CONSTRAINT FK_6631D52D8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE city_node ADD CONSTRAINT FK_6631D52DBF396750 FOREIGN KEY (id) REFERENCES world_map_node (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_log ADD CONSTRAINT FK_94657B00868E8BB9 FOREIGN KEY (lord_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE general ADD CONSTRAINT FK_CE29364A7E3C61F9 FOREIGN KEY (owner_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE general ADD CONSTRAINT FK_CE29364A766D5309 FOREIGN KEY (army_container_id) REFERENCES army_container (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE general ADD CONSTRAINT FK_CE29364A64D218E FOREIGN KEY (location_id) REFERENCES world_map_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE general ADD CONSTRAINT FK_CE29364A816C6140 FOREIGN KEY (destination_id) REFERENCES world_map_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_army ADD CONSTRAINT FK_80E7CE6CBC21F742 FOREIGN KEY (container_id) REFERENCES army_container (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_army ADD CONSTRAINT FK_80E7CE6C89329D25 FOREIGN KEY (resource_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE world_map_node ADD CONSTRAINT FK_E3B7CEA97E3C61F9 FOREIGN KEY (owner_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE world_map_node ADD CONSTRAINT FK_E3B7CEA9766D5309 FOREIGN KEY (army_container_id) REFERENCES army_container (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD lord_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D649868E8BB9 FOREIGN KEY (lord_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649868E8BB9 ON "user" (lord_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE general DROP CONSTRAINT FK_CE29364A766D5309');
        $this->addSql('ALTER TABLE unit_army DROP CONSTRAINT FK_80E7CE6CBC21F742');
        $this->addSql('ALTER TABLE world_map_node DROP CONSTRAINT FK_E3B7CEA9766D5309');
        $this->addSql('ALTER TABLE city_node DROP CONSTRAINT FK_6631D52DBF396750');
        $this->addSql('ALTER TABLE general DROP CONSTRAINT FK_CE29364A64D218E');
        $this->addSql('ALTER TABLE general DROP CONSTRAINT FK_CE29364A816C6140');
        $this->addSql('DROP SEQUENCE army_container_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE game_log_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE general_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE unit_army_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE world_map_node_id_seq CASCADE');
        $this->addSql('DROP TABLE army_container');
        $this->addSql('DROP TABLE city_node');
        $this->addSql('DROP TABLE game_log');
        $this->addSql('DROP TABLE general');
        $this->addSql('DROP TABLE unit_army');
        $this->addSql('DROP TABLE world_map_node');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D649868E8BB9');
        $this->addSql('DROP INDEX UNIQ_8D93D649868E8BB9');
        $this->addSql('ALTER TABLE "user" DROP lord_id');
    }
}
