var $hxClasses = $hxClasses || {};
var HxOverrides = $hxClasses["HxOverrides"] = function() { }
HxOverrides.__name__ = ["HxOverrides"];
HxOverrides.dateStr = function(date) {
	let m = date.getMonth() + 1;
	let d = date.getDate();
	let h = date.getHours();
	let mi = date.getMinutes();
	let s = date.getSeconds();
	return date.getFullYear() + "-" + (m < 10?"0" + m:"" + m) + "-" + (d < 10?"0" + d:"" + d) + " " + (h < 10?"0" + h:"" + h) + ":" + (mi < 10?"0" + mi:"" + mi) + ":" + (s < 10?"0" + s:"" + s);
}
HxOverrides.strDate = function(s) {
	switch(s.length) {
	case 8:
		var k = s.split(":");
		let d = new Date();
		d.setTime(0);
		d.setUTCHours(k[0]);
		d.setUTCMinutes(k[1]);
		d.setUTCSeconds(k[2]);
		return d;
	case 10:
		var k = s.split("-");
		return new Date(k[0],k[1] - 1,k[2],0,0,0);
	case 19:
		var k = s.split(" ");
		let y = k[0].split("-");
		let t = k[1].split(":");
		return new Date(y[0],y[1] - 1,y[2],t[0],t[1],t[2]);
	default:
		throw "Invalid date format : " + s;
	}
}
HxOverrides.cca = function(s,index) {
	let x = s.cca(index);
	if(x !== x) return undefined;
	return x;
}
HxOverrides.substr = function(s,pos,len) {
	if(pos !== null && pos !== 0 && len !== null && len < 0) return "";
	if(len === null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
}
HxOverrides.remove = function(a,obj) {
	let i = 0;
	let l = a.length;
	while(i < l) {
		if(a[i] === obj) {
			a.splice(i,1);
			return true;
		}
		i++;
	}
	return false;
}
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
}
var List = $hxClasses["List"] = function() {
	this.length = 0;
};
List.__name__ = ["List"];
List.prototype = {
	map: function(f) {
		var b = new List();
		var l = this.h;
		while(l != null) {
			var v = l[0];
			l = l[1];
			b.add(f(v));
		}
		return b;
	}
	,filter: function(f) {
		var l2 = new List();
		var l = this.h;
		while(l != null) {
			var v = l[0];
			l = l[1];
			if(f(v)) l2.add(v);
		}
		return l2;
	}
	,join: function(sep) {
		var s = new StringBuf();
		var first = true;
		var l = this.h;
		while(l != null) {
			if(first) first = false; else s.b += Std.string(sep);
			s.b += Std.string(l[0]);
			l = l[1];
		}
		return s.b;
	}
	,toString: function() {
		var s = new StringBuf();
		var first = true;
		var l = this.h;
		s.b += "{";
		while(l != null) {
			if(first) first = false; else s.b += ", ";
			s.b += Std.string(Std.string(l[0]));
			l = l[1];
		}
		s.b += "}";
		return s.b;
	}
	,iterator: function() {
		return { h : this.h, hasNext : function() {
				return this.h != null;
			}, next : function() {
				if(this.h == null) return null;
				var x = this.h[0];
				this.h = this.h[1];
				return x;
			}};
	}
	,remove: function(v) {
		var prev = null;
		var l = this.h;
		while(l != null) {
			if(l[0] === v) {
				if(prev == null) this.h = l[1]; else prev[1] = l[1];
				if(this.q === l) this.q = prev;
				this.length--;
				return true;
			}
			prev = l;
			l = l[1];
		}
		return false;
	}
	,clear: function() {
		this.h = null;
		this.q = null;
		this.length = 0;
	}
	,isEmpty: function() {
		return this.h == null;
	}
	,pop: function() {
		if(this.h == null) return null;
		var x = this.h[0];
		this.h = this.h[1];
		if(this.h == null) this.q = null;
		this.length--;
		return x;
	}
	,last: function() {
		return this.q == null?null:this.q[0];
	}
	,first: function() {
		return this.h == null?null:this.h[0];
	}
	,push: function(item) {
		var x = [item,this.h];
		this.h = x;
		if(this.q == null) this.q = x;
		this.length++;
	}
	,add: function(item) {
		var x = [item];
		if(this.h == null) this.h = x; else this.q[1] = x;
		this.q = x;
		this.length++;
	}
	,length: null
	,q: null
	,h: null
	,__class__: List
}
var haxe = haxe || {}
haxe.Timer = $hxClasses["haxe.Timer"] = function(time_ms) {
	let me = this;
	this.id = setInterval(function() {
		me.run();
	},time_ms);
};
haxe.Timer.__name__ = ["haxe","Timer"];
haxe.Timer.delay = function(f,time_ms) {
	var t = new haxe.Timer(time_ms);
	t.run = function() {
		t.stop();
		f();
	};
	return t;
}
haxe.Timer.measure = function(f,pos) {
	let t0 = haxe.Timer.stamp();
	let r = f();
	haxe.Log.trace(haxe.Timer.stamp() - t0 + "s",pos);
	return r;
}
haxe.Timer.stamp = function() {
	return new Date().getTime() / 1000;
}
haxe.Timer.prototype = {
	run: function() {
	}
	,stop: function() {
		if(this.id == null) return;
		clearInterval(this.id);
		this.id = null;
	}
	,id: null
	,__class__: haxe.Timer
}
var js = js || {}
js.Lib = $hxClasses["js.Lib"] = function() { }
js.Lib.__name__ = ["js","Lib"];
js.Lib.document = null;
js.Lib.window = null;
var mt = mt || {}
if(!mt.js) mt.js = {}
mt.js.Tip = $hxClasses["mt.js.Tip"] = function() { }
mt.js.Tip.__name__ = ["mt","js","Tip"];
mt.js.Tip.lastRef = null;
mt.js.Tip.placeRef = null;
mt.js.Tip.initialized = null;
mt.js.Tip.tooltip = null;
mt.js.Tip.tooltipContent = null;
mt.js.Tip.mousePos = null;
mt.js.Tip.onHide = null;
mt.js.Tip.excludeList = null;
mt.js.Tip.show = function(refObj,contentHTML,cName,pRef) {
	mt.js.Tip.init();
	if(mt.js.Tip.tooltip == null) {
		mt.js.Tip.tooltip = js.Lib.document.getElementById(mt.js.Tip.tooltipId);
		if(mt.js.Tip.tooltip == null) {
			mt.js.Tip.tooltip = js.Lib.document.createElement("div");
			mt.js.Tip.tooltip.id = mt.js.Tip.tooltipId;
			js.Lib.document.body.insertBefore(mt.js.Tip.tooltip,js.Lib.document.body.firstChild);
		}
		mt.js.Tip.tooltip.style.top = "-1000px";
		mt.js.Tip.tooltip.style.position = "absolute";
		mt.js.Tip.tooltip.style.zIndex = mt.js.Tip.tipZIndex;
	}
	if(mt.js.Tip.tooltipContent == null) {
		mt.js.Tip.tooltipContent = js.Lib.document.getElementById(mt.js.Tip.tooltipContentId);
		if(mt.js.Tip.tooltipContent == null) {
			mt.js.Tip.tooltipContent = js.Lib.document.createElement("div");
			mt.js.Tip.tooltipContent.id = mt.js.Tip.tooltipContentId;
			mt.js.Tip.tooltip.appendChild(mt.js.Tip.tooltipContent);
		}
	}
	if(pRef == null) pRef = false;
	mt.js.Tip.placeRef = pRef;
	if(cName == null) mt.js.Tip.tooltip.className = mt.js.Tip.defaultClass; else mt.js.Tip.tooltip.className = cName;
	if(mt.js.Tip.lastRef != null && mt.js.Tip.onHide != null) {
		mt.js.Tip.onHide();
		mt.js.Tip.onHide = null;
	}
	mt.js.Tip.lastRef = refObj;
	mt.js.Tip.tooltipContent.innerHTML = contentHTML;
	if(mt.js.Tip.placeRef) mt.js.Tip.placeTooltipRef(); else mt.js.Tip.placeTooltip();
}
mt.js.Tip.exclude = function(id) {
	let e = js.Lib.document.getElementById(id);
	if(e == null) throw id + " not found";
	if(mt.js.Tip.excludeList == null) mt.js.Tip.excludeList = new List();
	mt.js.Tip.excludeList.add(e);
}
mt.js.Tip.placeTooltip = function() {
	if(mt.js.Tip.mousePos == null) return;
	let tts = mt.js.Tip.elementSize(mt.js.Tip.tooltip);
	let w = mt.js.Tip.windowSize();
	let left = mt.js.Tip.mousePos.x + mt.js.Tip.xOffset;
	let top = mt.js.Tip.mousePos.y + mt.js.Tip.yOffset;
	if(top + tts.height > w.height - 2 + w.scrollTop) {
		if(mt.js.Tip.mousePos.y - tts.height > 5 + w.scrollTop) top = mt.js.Tip.mousePos.y - tts.height - 5; else top = w.height - 2 + w.scrollTop - tts.height;
	}
	if(left + tts.width > w.width - 22 + w.scrollLeft) {
		if(mt.js.Tip.mousePos.x - tts.width > 5 + w.scrollLeft) left = mt.js.Tip.mousePos.x - tts.width - 5; else left = w.width - 22 + w.scrollLeft - tts.width;
	}
	if(top < 0) top = 0;
	if(left < 0) left = 0;
	if(mt.js.Tip.excludeList != null) {
		let $it0 = mt.js.Tip.excludeList.iterator();
		while( $it0.hasNext() ) {
			let e = $it0.next();
			let s = mt.js.Tip.elementSize(e);
			if(left > s.x + s.width || left + tts.width < s.x || top > s.y + s.height || top + tts.height < s.y) continue;
			let dx1 = left - (s.x + s.width);
			let dx2 = left + tts.width - s.x;
			let dx = Math.abs(dx1) > Math.abs(dx2)?dx2:dx1;
			let dy1 = top - (s.y + s.height);
			let dy2 = top + tts.height - s.y;
			let dy = Math.abs(dy1) > Math.abs(dy2)?dy2:dy1;
			let cx = left + tts.width / 2 - mt.js.Tip.mousePos.x;
			let cy = top + tts.height / 2 - mt.js.Tip.mousePos.y;
			if((cx - dx) * (cx - dx) + cy * cy > cx * cx + (cy - dy) * (cy - dy)) top -= dy; else left -= dx;
		}
	}
	mt.js.Tip.tooltip.style.left = left + "px";
	mt.js.Tip.tooltip.style.top = top + "px";
}
mt.js.Tip.placeTooltipRef = function() {
	let o = mt.js.Tip.elementSize(mt.js.Tip.lastRef);
	let tts = mt.js.Tip.elementSize(mt.js.Tip.tooltip);
	if(o.width <= 0) mt.js.Tip.tooltip.style.left = o.x + "px"; else mt.js.Tip.tooltip.style.left = o.x - tts.width * 0.5 + o.width * 0.5 + "px";
	mt.js.Tip.tooltip.style.top = o.y + Math.max(mt.js.Tip.minOffsetY,o.height) + "px";
}
mt.js.Tip.showTip = function(refObj,title,contentBase) {
	contentBase = "<p>" + contentBase + "</p>";
	mt.js.Tip.show(refObj,"<div class=\"title\">" + title + "</div>" + contentBase);
}
mt.js.Tip.hide = function() {
	if(mt.js.Tip.lastRef == null) return;
	mt.js.Tip.lastRef = null;
	if(mt.js.Tip.onHide != null) {
		mt.js.Tip.onHide();
		mt.js.Tip.onHide = null;
	}
	mt.js.Tip.tooltip.style.top = "-1000px";
	mt.js.Tip.tooltip.style.width = "";
}
mt.js.Tip.clean = function() {
	if(mt.js.Tip.lastRef == null) return;
	if(mt.js.Tip.lastRef.parentNode == null) return mt.js.Tip.hide();
	if(mt.js.Tip.lastRef.id !== null && mt.js.Tip.lastRef.id !== "") {
		if(js.Lib.document.getElementById(mt.js.Tip.lastRef.id) !== mt.js.Tip.lastRef) return mt.js.Tip.hide();
	}
}
mt.js.Tip.elementSize = function(o) {
	let ret = { x : 0, y : 0, width : o.clientWidth, height : o.clientHeight};
	let p = o;
	while(p !== null) {
		if(p.offsetParent !== null) {
			ret.x += p.offsetLeft - p.scrollLeft;
			ret.y += p.offsetTop - p.scrollTop;
		} else {
			ret.x += p.offsetLeft;
			ret.y += p.offsetTop;
		}
		p = p.offsetParent;
	}
	return ret;
}
mt.js.Tip.windowSize = function() {
	let ret = { x : 0, y : 0, width : js.Lib.window.innerWidth, height : js.Lib.window.innerHeight, scrollLeft : js.Lib.document.body.scrollLeft + js.Lib.document.documentElement.scrollLeft, scrollTop : js.Lib.document.body.scrollTop + js.Lib.document.documentElement.scrollTop};
	let isIE = document.all != null && window.opera == null;
	let body = isIE?js.Lib.document.documentElement:js.Lib.document.body;
	if(ret.width == null) ret.width = body.clientWidth;
	if(ret.height == null) ret.height = body.clientHeight;
	return ret;
}
mt.js.Tip.onMouseMove = function(evt) {
	try {
		var posx = 0;
		var posy = 0;
		if(evt == null) evt = js.Lib.window.event;
		var e = evt;
		if(e.pageX || e.pageY) {
			posx = e.pageX;
			posy = e.pageY;
		} else if(e.clientX || e.clientY) {
			posx = e.clientX + js.Lib.document.body.scrollLeft + js.Lib.document.documentElement.scrollLeft;
			posy = e.clientY + js.Lib.document.body.scrollTop + js.Lib.document.documentElement.scrollTop;
		}
		mt.js.Tip.mousePos = { x : posx, y : posy};
		if(mt.js.Tip.lastRef != null && !mt.js.Tip.placeRef) mt.js.Tip.placeTooltip();
	} catch( e ) {
	}
}
mt.js.Tip.trackMenu = function(elt,onOut) {
	mt.js.Tip.init();
	var ftrack = null;
	var body = js.Lib.document.body;
	ftrack = function(evt) {
		if(mt.js.Tip.mousePos == null) return;
		var size = mt.js.Tip.elementSize(elt);
		if(mt.js.Tip.mousePos.x < size.x || mt.js.Tip.mousePos.y < size.y || mt.js.Tip.mousePos.x > size.x + size.width || mt.js.Tip.mousePos.y > size.y + size.height) {
			if(body.attachEvent) body.detachEvent("onmousemove",ftrack); else body.removeEventListener("mousemove",ftrack,false);
			onOut();
		}
	};
	if(body.attachEvent) body.attachEvent("onmousemove",ftrack); else body.addEventListener("mousemove",ftrack,false);
}
mt.js.Tip.init = function() {
	if(mt.js.Tip.initialized) return;
	if(document.body != null) {
		mt.js.Tip.initialized = true;
		document.body.onmousemove = mt.js.Tip.onMouseMove;
	}
}
mt.js.Timer = $hxClasses["mt.js.Timer"] = function(now,end,start) {
	this.t = now.getTime();
	this.start = start == null?now:start;
	this.end = end;
	if(mt.js.Timer.timer == null) {
		mt.js.Timer.timer = new haxe.Timer(1000);
		mt.js.Timer.timer.run = function() {
			var _g = 0, _g1 = mt.js.Timer.timers;
			while(_g < _g1.length) {
				var t = _g1[_g];
				++_g;
				t.update();
			}
		};
	}
	mt.js.Timer.timers.push(this);
};
mt.js.Timer.__name__ = ["mt","js","Timer"];
mt.js.Timer.timer = null;
mt.js.Timer.alloc = function(now,end,prec,div) {
	if(div == null) {
		div = "timer_" + mt.js.Timer.timers.length;
		js.Lib.document.write("<div id=\"" + div + "\" class=\"timer\"></div>");
	}
	var t = new mt.js.Timer(HxOverrides.strDate(now),HxOverrides.strDate(end));
	t.textDiv = { id : div, prec : prec};
	t.update();
	return t;
}
mt.js.Timer.prototype = {
	onUpdate: function() {
	}
	,onReady: function() {
		if(this.rem.time < -2) {
			js.Lib.window.location = js.Lib.window.location;
			this.onReady = function() {
			};
		}
	}
	,update: function() {
		this.t += 1000;
		var remt = (this.end.getTime() - this.t) / 1000;
		var rt = remt < 0?0:remt;
		this.rem = { days : rt / 86400 | 0, hours : (rt / 3600 | 0) % 24, minutes : (rt / 60 | 0) % 60, seconds : rt % 60 | 0, time : remt};
		var et = this.end.getTime();
		var st = this.start.getTime();
		this.progress = this.t >= et?1:(this.t - st) / (et - st);
		if(this.textDiv != null) {
			let div = js.Lib.document.getElementById(this.textDiv.id);
			if(div != null) div.innerHTML = this.buildText();
		}
		if(this.progressDiv != null) {
			let div = js.Lib.document.getElementById(this.progressDiv.id);
			if(div != null) {
				let w = this.progressDiv.width * this.progress | 0;
				div.style.width = w + "px";
			}
		}
		if(remt <= 0) this.onReady();
		this.onUpdate();
	}
	,buildText: function() {
		var str = "";
		var prec = this.textDiv.prec;
		var force = false;
		if(prec < 1) {
			let sep = this.rem.seconds % 2 === 0?":":"<span style=\"opacity : 0\">:</span>";
			if(this.rem.hours > 0) {
				let str1 = this.rem.hours + sep;
				if(this.rem.minutes < 10) str1 += "0";
				return str1 + this.rem.minutes;
			}
			let str1 = this.rem.minutes + sep;
			if(this.rem.seconds < 10) str1 += "0";
			return str1 + this.rem.seconds;
		}
		if(this.rem.days > 0) {
			str += this.rem.days + mt.js.Timer.TIMES.charAt(0) + " ";
			force = true;
			if(--prec === 0) return str;
		}
		if(force || this.rem.hours > 0) {
			str += this.rem.hours + mt.js.Timer.TIMES.charAt(1) + " ";
			force = true;
			if(--prec === 0) return str;
		}
		if(force || this.rem.minutes > 0) {
			if(force && this.rem.minutes < 10) str += "0";
			str += this.rem.minutes + mt.js.Timer.TIMES.charAt(2) + " ";
			force = true;
			if(--prec === 0) return str;
		}
		if(force && this.rem.seconds < 10) str += "0";
		str += this.rem.seconds + mt.js.Timer.TIMES.charAt(3) + " ";
		return str;
	}
	,stop: function() {
		HxOverrides.remove(mt.js.Timer.timers,this);
	}
	,progressDiv: null
	,textDiv: null
	,rem: null
	,progress: null
	,end: null
	,start: null
	,t: null
	,__class__: mt.js.Timer
}
js.App = $hxClasses["js.App"] = function() { }
js.App.__name__ = ["js","App"];
js.App.get = function(id) {
	return js.Lib.document.getElementById(id);
}
js.App.autoButton = function(id) {
	js.Lib.document.onkeydown = function(e) {
		if(e == null) e = event;
		if(e.keyCode == 27 || e.keyCode == 13) {
			js.Lib.document.onkeydown = function(e1) {
			};
			let d = js.App.get(id);
			if(d != null) {
				if(d.onclick == null || d.onclick() === true) haxe.Timer.delay(function() {
					js.Lib.document.location = d.href;
				},10);
			}
		}
	};
}
js.App.fill = function(e,html) {
	if(HxOverrides.substr(html,0,5) === "<!DOC") {
		let r = new EReg("<body[^>]*>([^�]*)</body>","");
		js.App.getBody().innerHTML = r.match(html)?r.matched(1):html;
		let top = js.App.get("mxtop");
		if(top != null) top.style.display = "";
	} else e.innerHTML = html;
}
js.App.show = function(id) {
	js.App.get(id).style.display = "";
	return false;
}
js.App.getBody = function() {
	return js.Lib.document.getElementsByTagName("body")[0];
}
js.App.refresh = function() {
	js.Lib.window.location.reload(true);
	return false;
}
js.App.hideNotification = function(hasNext) {
	if(hasNext) return js.App.refresh();
	js.App.get("notification").style.display = "none";
	return false;
}
js.App.evaluateJS = function(id) {
	var e = js.App.get(id);
	if(e == null) return;
	var scripts = js.Lib.document.getElementsByTagName("script");
	var body = js.App.getBody();
	var _g1 = 0, _g = scripts.length;
	while(_g1 < _g) {
		var i = _g1++;
		var s = scripts[i];
		var p = s.parentNode;
		if(s.innerHTML == "") continue;
		while(p != null && p != body) {
			if(p == e) {
				eval(s.innerHTML);
				break;
			}
			p = p.parentNode;
		}
	}
}
js.App.twinCheck = function() {
	try {
		if( typeof(_tid) != undefined ) _tid.onLoad();
	} catch( e ) {
	}
}
js.App.reload = function(id,url,reloadId) {
	var rel = js.App.get(reloadId == null?id:reloadId);
	if(rel != null) rel.innerHTML = "<div class=\"reload\"></div>";
	var doc = js.App.get(id);
	if(doc == null) throw "No such element '" + id + "'";
	var data = null, timer = rel == null;
	var h = new haxe.Http(url);
	var setData = function() {
		mt.js.Tip.hide();
		js.App.fill(doc,data);
		haxe.Timer.delay((function(f,id1) {
			return function() {
				return f(id1);
			};
		})(js.App.evaluateJS,id),50);
		js.App.twinCheck();
	};
	h.onData = function(d) {
		data = d;
		if(timer) setData();
	};
	if(!timer) haxe.Timer.delay(function() {
		timer = true;
		if(data != null) setData();
	},800);
	h.request(false);
	return false;
}
js.App.main = function() {
	_ = js.App;
}

if(typeof document != "undefined") js.Lib.document = document;
if(typeof window != "undefined")
{
	js.Lib.window = window;
	js.Lib.window.onerror = function(msg,url,line) {
		let f = js.Lib.onerror;
		if(f == null) return false;
		return f(msg,[url + ":" + line]);
	};
}
js.Lib.onerror = null;

mt.js.Tip.init();
mt.js.Tip.xOffset = 3;
mt.js.Tip.yOffset = 22;
mt.js.Tip.defaultClass = "normalTip";
mt.js.Tip.tooltipId = "tooltip";
mt.js.Tip.tooltipContentId = "tooltipContent";
mt.js.Tip.minOffsetY = 23;
mt.js.Tip.tipZIndex = 10;
mt.js.Timer.timers = [];
mt.js.Timer.TIMES = "jhms";

js.App.ref = [mt.js.Tip, mt.js.Timer];
js.App.main();
