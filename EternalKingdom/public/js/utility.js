const debugServerRequest = false;
const debugPayload = false;

//#region Server request

/*
 * Triggers a POST request with the specified payload and call the callback when request succeeded!
 */
function ServerPost(path, payload, callback)
{
    if(debugServerRequest)
    {
        console.log("[ServerPOST] URL:" + path);
    }

    if(debugPayload)
    {
        // I separated those 2 lines to be able to manipulate the json object in the debug console!
        console.log("[ServerPost] Payload:\n");
        console.log(payload);
    }

    let data = new FormData();
    data.append("json", JSON.stringify(payload));

    fetch(path, {
        method: "POST",
        body  : data
    })
        .then(function (res)
        {
            return res.json();
        })
        .then(function (data)
        {
            if(debugServerRequest)
            {
                console.log("[Server Response]\n" + JSON.stringify(data));
            }
            callback(data);
        })
        .catch(function (error)
        {
            console.error("[Server Error]\n" + error);
        });
}

/*
 * Triggers a GET request and call the callback when request succeeded!
 */
function ServerGet(path, callback)
{
    if(debugServerRequest)
    {
        console.log("[ServerGET] URL:" + path);
    }

    fetch(path, {
        method: "GET"
    })
        .then(function (response)
        {
            if (response.status !== 200)
            {
                throw "Invalid response (" + response.status + "): " + response.statusText;
            }

            const contentType = response.headers.get("content-type");
            if (contentType !== null && contentType.indexOf("application/json") !== -1)
            {
                let data = response.json();
                if(debugServerRequest)
                {
                    console.log("[Server Response]\n" + JSON.stringify(data));
                }
                callback(data);
            }

            callback(response);
        })
        .catch(function (error)
        {
            console.error("[Server Error]\n" + error);
        });
}

//#endregion

//#region Maths

function lerp(start, end, t)
{
    return start * (1.0 - t) + end * t;
}

function clamp(number, min, max)
{
    return Math.max(min, Math.min(number, max));
}

// Get random integer between 0 and max
function GetRandomInRange(a_max)
{
    return Math.floor(Math.random() * a_max);
}

//#endregion