// the nice visible canvas with the map
var view; var viewContext;
// hidden selection cavnas used to know what the mouse/finger is selecting
var selectionContext; 
// the tooltip with informations about a city or a general on mouve hover
var tooltip; 
// we don't want to draw the tooltip when the mouse is out of the map frame, or when dragging
var hideTooltip = false; 

// we cannot see the entire map, so we need scrolling.
// Actually currentScroll is the position of the point of map which is at center of the view ((0,0) is up left corner)
// scrollMin and scrollMax prevent the user from scrolling far outside the map
var currentScroll = {x : 0,y : 0}; 
var scrollMin = {};
var scrollMax = {};

// we need to keep trace of previous mouse (or touch) position when dragging
var lastClientX = 0;
var lastClientY = 0;
// we need to prevent click event after dragging, so we need to difference 'click' vs 'mousedown, dragging and mouseup'
var hasDragged = false;

//we need to prevent the whole page scrolling when scrolling the map on tactile devices
var isPageScrollable = true;

// 1/zoom is the magnification. The larger is the zoom, the larger part of the map you can see.
var zoom = 1, zoomMax;

//what the mouse is hovering, or is clicking?
var selectedEntity = {
    id : -1, //the id of the entity; -1 means no entity is selected
    type : '', //can be city or general
    htmlData : '', //the data we will show in tooltip
    x : 0, //x coordinate of the entity (in view coordinates)
    y : 0 //y coordinate of the entity (in view coordinates)
}

//shows the current kingdoms with colors
var isStrategicView = false; 

//paths to the images used to render the map
var imagesSrc = {
    'ground'    : 'img/map/ground_layer.png',
    'landscape' : 'img/map/landscape_sprites.png',
    'lake'      : 'img/map/lake.png',
    'background': 'img/map/map_background.jpg'
}
//map from image names to Image objects
var loadedImages = {}

//var lastLoop = new Date(); //used to measure framerate

// To improve performances, the map is divided in chunks.
// Each chunk is prerendered is an offscreen canvas. 
// We need enought offscreen canvases to cover the view: nbChunksViewX in x dimension, nbChunksViewY in y
// currentFirstColumn and currentFirstLine are indices or the upper right chunk
var offscreenCanvasArray;
var nbChunksViewX, nbChunksViewY, chunkSize;
var currentFirstColumn = 0;
var currentFirstLine = 0;

// list of colors for kingdoms in strategic view
// https://en.wikipedia.org/wiki/Help:Distinguishable_colors
var kingdomColors = [
    '#F0A3FF','#0075DC','#993F00','#4C005C','#191919','#005C31','#2BCE48',
    '#FFCC99','#808080','#94FFB5','#8F7C00','#9DCC00','#C20088','#003380',
    '#FFA405','#FFA8BB','#426600','#FF0010','#5EF1F2','#00998F','#E0FF66',
    '#740AFF','#990000','#FFFF80','#FFFF00','#FF5005'];

// map with players to colors
var playerIdToColorId;

// the graph with cities and the paths between them
var mapGraph;
// list of players, cities with theirs garrisons: everything that changes during the game!
var gameState;
// limits of Voronoi cells for each cities, position of trees, mountains: everything that make the map beautiful!
var renderData;


// call init when DOM is loaded
if  (document.readyState === "complete" || 
    (document.readyState !== "loading" && !document.documentElement.doScroll)) {
    init();
} else {
    document.addEventListener("DOMContentLoaded", init);
}


function init() {
    tooltip = document.getElementById('map_tip');
    tooltip.style.display = 'none';

    view = document.getElementById('map_view');
    viewContext = view.getContext("2d");
    let selection = document.getElementById('map_click');
    selectionContext = selection.getContext("2d");

    let parentRect = view.parentNode.getBoundingClientRect();
    view.width = parentRect.width;
    view.height = parentRect.height;
    selection.width = parentRect.width;
    selection.height = parentRect.height;    


    let loadJsonAndImg = async function() {
        //TODO : manage map directory
        let graphUrl = '../maps/testgen/graph.json';
        let kingdomsUrl = '../maps/testgen/gameState.json';
        let renderDataUrl = '../maps/testgen/renderData.json';

        const [graphResponse, gameStateResponse, renderDataResponse] = 
            await Promise.all([fetch(graphUrl), fetch(kingdomsUrl), fetch(renderDataUrl)]);

        mapGraph = await graphResponse.json();
        gameState = await gameStateResponse.json();
        renderData = await renderDataResponse.json();


        for(let key in imagesSrc) {
            let img = new Image();
            img.src = imagesSrc[key];
            await img.decode();
            loadedImages[key] = img;
        } 

        await initHtilesSpriteSheets();
    }

    loadJsonAndImg().then(() => {
        initZoom();
        initPlayerToColorArray();
        initOffscreenCanvases();
        updateBackground(true);
        draw();
    })

    initEventListeners();
}

function initZoom() {
    zoomMax = 1;
    while ((renderData['map_width'] / zoomMax > view.width)&&(renderData['map_height'] / zoomMax > view.height)) {
        zoomMax *= 2;
    }
    updateScrollWithZoom();
}

function initPlayerToColorArray() {
    playerIdToColorId = new Array(gameState["players"].length);
    gameState["players"].forEach(function (playerName, i) {
        playerIdToColorId[i] = nameToColorId(playerName);
    });

}

/**
 * Generate a set of pregenerated tiles in a sub canvas. These tiles are build
 * with several sprites. For landscapes with lots of small sprites, it is more
 * efficient to draw a small amount of tiles than to draw independently every sprite.
 */
async function initHtilesSpriteSheets() {
    for (let htilesSource in renderData["htiles"]) {
        let htilesParameters = renderData["htiles"][htilesSource];
        loadedImages[htilesSource] = document.createElement('canvas');
        loadedImages[htilesSource].width = htilesParameters['tilesheet_width'];
        loadedImages[htilesSource].height = htilesParameters['tilesheet_height'];
        let context = loadedImages[htilesSource].getContext('2d');
        
        let tileWidth = htilesParameters['tile_width'];
        let tileHeight = htilesParameters['tile_height'];

        let sourceImg = loadedImages[htilesParameters['source']];
        for (const tile of htilesParameters['tiles']) {
            let x_in_tilesheet = tile["x"];
            let y_in_tilesheet = tile["y"];
            context.save();
            context.beginPath();
            context.rect(x_in_tilesheet, y_in_tilesheet, tileWidth, tileHeight);
            context.clip();

            for (const spriteIdAndPos of tile['sprites']) {
                let spriteData = renderData["sprites"][htilesParameters['source']][spriteIdAndPos[0]];
                context.drawImage(sourceImg, 
                    spriteData[0], spriteData[1], spriteData[2], spriteData[3],
                    spriteIdAndPos[1], spriteIdAndPos[2], spriteData[2], spriteData[3]);
            }
            context.restore();
        }
    }
}

function initOffscreenCanvases() {
    chunkSize = renderData["map_chunks"]["chunk_size"];

    nbChunksViewX = Math.ceil(view.width/chunkSize) + 1;
    nbChunksViewY = Math.ceil(view.height/chunkSize) + 1;

    offscreenCanvasArray = new Array(nbChunksViewX);
    for(var i=0; i<nbChunksViewX; i++) { 
        offscreenCanvasArray[i] = new Array(nbChunksViewY);
        for(var j=0; j<nbChunksViewY; j++) {
            offscreenCanvasArray[i][j] = document.createElement('canvas');
            offscreenCanvasArray[i][j].width = chunkSize;
            offscreenCanvasArray[i][j].height = chunkSize;
            let context = offscreenCanvasArray[i][j].getContext('2d');
            context.fillStyle = context.createPattern(loadedImages['background'], 'repeat');
        }
    }
}


function initEventListeners() {
    view.addEventListener('mousemove', onMouseMove);
    view.addEventListener('mousedown', onMouseDragStart);
    view.addEventListener('touchstart', onTouchDragStart);
    view.addEventListener('mouseleave', onMouseLeave);    
    view.addEventListener('mouseover', onMouseOver);
    view.addEventListener('click', onClickMap);

    var preventScrolling = function(e) {
        if (! isPageScrollable) {
            e.preventDefault();
        }
    }    
    document.addEventListener('touchmove', preventScrolling, { passive:false });
    document.getElementById('zoom_out').addEventListener('click', onClickZoomOut);
    document.getElementById('zoom_in').addEventListener('click', onClickZoomIn);
    document.getElementById('toggle_strat_mode').addEventListener('click', onToggleStratMode);
}


/**
 * Return a color for a kingdom dependent on the lord's name
 * @param {string} name - the name of the lord
 */
function nameToColorId(name) {
    colorId = 0
    for (let i = 0; i < name.length; i++) {
        colorId += name[i].charCodeAt(0);
    }
    return (colorId % kingdomColors.length)
}


/**
 * Update the background in offscreen canvases (only render visible chunks of the map)
 * Background is the non-animated part of the map (cities, paths, trees, lakes, etc)
 * 
 * @param {boolean} forceUpdate - If forceUpdate, update every visible chunk. 
 * Else only update chunks that was not visible at previous frame (that have not yet been rendered)
 */
function updateBackground(forceUpdate) {
    let firstColumn =  Math.floor((currentScroll.x - view.width * zoom / 2) / (chunkSize * zoom));
    let firstLine = Math.floor((currentScroll.y - view.height * zoom / 2) / (chunkSize * zoom));

    let deltaColumn = firstColumn - currentFirstColumn;
    let deltaLine = firstLine - currentFirstLine;

    for(let i=0 ; i<nbChunksViewX ; i++) {
        for(let j=0 ; j<nbChunksViewY ; j++) {
            if(forceUpdate || (i+deltaColumn < 0) || (i+deltaColumn >= nbChunksViewX) || (j+deltaLine < 0) || (j+deltaLine >= nbChunksViewY)) {
                renderChunk(firstColumn+i, firstLine+j, (firstColumn+i+nbChunksViewX)%nbChunksViewX, (firstLine+j+nbChunksViewY)%nbChunksViewY);
            }
         }       
    }

    currentFirstColumn = firstColumn;
    currentFirstLine = firstLine;

    updateSelection();
}


/**
 * Render a chunk of the map in an offscreen canvas
 * 
 * @param {number} mapColumn - position (x axis) of the chunk in the map
 * @param {number} mapLine   - position (y axis) of the chunk in the map
 * @param {number} i - column index in offscreenCanvasArray
 * @param {number} j - line index in offscreenCanvasArray
 */
function renderChunk(mapColumn, mapLine, i, j) {
    let context = offscreenCanvasArray[i][j].getContext('2d');
    context.save();

    var zoomfactor = 1/zoom
    let zoomedChunkSize = chunkSize * zoom;

    context.setTransform (zoomfactor, 0, 0, zoomfactor, -mapColumn * chunkSize, -mapLine * chunkSize);
    context.fillRect(mapColumn * zoomedChunkSize, mapLine * zoomedChunkSize, zoomedChunkSize, zoomedChunkSize);
    renderBackground(context, mapColumn, mapLine);

    context.restore();
}


/**
 * Render the background of a chunk
 * 
 * @param {Object} context - the canvas context to render the chunk
 * @param {number} column - position (x axis) of the chunk in the map
 * @param {number} line   - position (y axis) of the chunk in the map
 */
function renderBackground(context, column, line) {
    renderLandscape(context, column, line);
    renderCities(context);
    renderPaths(context);
    if(isStrategicView) {
        renderKingdoms(context);
    }
}



/**
 * Render the landscape in a chunk: lakes, trees, mountains, etc
 * 
 * @param {Object} context - the canvas context to render the chunk
 * @param {number} column - position (x axis) of the chunk in the map
 * @param {number} line   - position (y axis) of the chunk in the map
 */
function renderLandscape(context, column, line) {
    let nbMapChunksX = renderData["map_chunks"]["nb_chunks_x"];
    let nbMapChunksY = renderData["map_chunks"]["nb_chunks_y"];

    for(let l=line*zoom;l<(line+1)*zoom;l++) {
        for(let c=column*zoom;c<(column+1)*zoom;c++) {

            if((c<0)||(c>=nbMapChunksX)||(l<0)||(l>=nbMapChunksY)){
                break;
            }
            
            context.save();
            context.beginPath();
            context.rect(c*chunkSize,l*chunkSize,chunkSize,chunkSize);
            context.clip();

            let chunk = renderData["map_chunks"]["chunks"][l * nbMapChunksY + c];
            for (const layer of chunk) {
                let source = layer["source"];
                for (const sprite of layer["sprites"]) {
                    let spriteData = renderData["sprites"][source][sprite[0]];
                    context.drawImage(loadedImages[source], 
                        spriteData[0], spriteData[1], spriteData[2], spriteData[3],
                        sprite[1], sprite[2], spriteData[2], spriteData[3]);
                }
            }
            context.restore();
        }
    }
}

/**
 * Render the paths between cities in a chunk
 * 
 * @param {Object} context - the canvas context to render the chunk
 */
function renderPaths(context) {
    for (const path of renderData["paths"]) {
        context.beginPath();
        context.setLineDash([2, 4]);
        context.moveTo(path[0],path[1]);
        context.lineTo(path[2],path[3]);
        context.stroke();
    }
}


/**
 * Render kingdoms in a chunk. This function is called when stategic view is activated
 * 
 * @param {Object} context - the canvas context to render the chunk
 */
function renderKingdoms(context) {
    context.save();
    context.globalAlpha = 0.5;
    
    let voronoiVertices = renderData["voronoi_vertices"];

    for(let i = 0; i < gameState["cities"].length; i++){
        let playerId = gameState["cities"][i];

        if(playerId != -1) {

            let colorId = playerIdToColorId[playerId];
            let regionVertices = renderData["voronoi_regions_vert"][i];
            context.beginPath();
            let vertexId = regionVertices[0];
            context.fillStyle = kingdomColors[colorId];

            context.moveTo(voronoiVertices[vertexId][0],voronoiVertices[vertexId][1]);
            for(let j = 1; j < regionVertices.length; j++) {
                vertexId = regionVertices[j];
                context.lineTo(voronoiVertices[vertexId][0],voronoiVertices[vertexId][1]);
            }
            context.fill();
        }
    }
    
    context.lineWidth = 3;
    context.setLineDash([]);

    for(let i = 0; i < gameState["cities"].length; i++){
        let playerId = gameState["cities"][i];
        let regionVertices = renderData["voronoi_regions_vert"][i];
        let vertexId = regionVertices[0];
    
        let adjPlayerId;
        let adjCityId = renderData["voronoi_regions_adj"][i][0];
        if(adjCityId >=0) {
            adjPlayerId = gameState["cities"][adjCityId];
        } else {
            adjPlayerId = -1;
        }

        for(let j = 1; j < regionVertices.length; j++) {
            if (playerId != adjPlayerId) {
                context.beginPath();
                context.moveTo(voronoiVertices[vertexId][0],voronoiVertices[vertexId][1]);
                vertexId = regionVertices[j];
                context.lineTo(voronoiVertices[vertexId][0],voronoiVertices[vertexId][1]);
                context.stroke();
            } else {
                vertexId = regionVertices[j];
            }
            adjCityId = renderData["voronoi_regions_adj"][i][j];
            if(adjCityId >=0) {
                adjPlayerId = gameState["cities"][adjCityId];
            } else {
                adjPlayerId = -1;
            }
        }

        if (playerId != adjPlayerId) {
            context.beginPath();
            context.moveTo(voronoiVertices[vertexId][0],voronoiVertices[vertexId][1]);
            vertexId = regionVertices[0];
            context.lineTo(voronoiVertices[vertexId][0],voronoiVertices[vertexId][1]);
            context.stroke();
        }
    }

    context.restore();
}


/**
 * Render cities in a chunk. //TODO: tower and houses
 * 
 * @param {Object} context - the canvas context to render the chunk
 */
function renderCities(context) {
    for(const city of mapGraph) {
        let cityData;
        if(city["cap"]) {
            cityData = renderData["cities_sprites"]["sprites"]["towers"][0];
        } else {
            cityData = renderData["cities_sprites"]["sprites"]["city"];
        }
        context.drawImage(loadedImages[renderData["cities_sprites"]["source"]], 
            cityData[0], cityData[1], cityData[2], cityData[3],
            city["x"] - cityData[4],
            city["y"] - cityData[5],
            cityData[2], cityData[3]);
    }
}


/**
 * Draw selectable entities in selection canvas. Each entity has a single color.
 */
function updateSelection() {
    selectionContext.save();
    var zoomfactor = 1/zoom;

    selectionContext.setTransform (
        zoomfactor, 0, 0, zoomfactor,
        view.width / 2 - currentScroll.x * zoomfactor,
        view.height / 2 - currentScroll.y * zoomfactor);

    selectionContext.fillRect(
        currentScroll.x - view.width/2 * zoom, currentScroll.y - view.height/2 * zoom,
        view.width * zoom, view.height * zoom);

    for(let cityId = 0; cityId < mapGraph.length; cityId++){
        let city = mapGraph[cityId];

        var r = cityId % 256;
        var b = Math.floor(cityId / 65536);
        var g = Math.floor((cityId - 256*b)/256);
        b = b+1;
        selectionContext.fillStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
        let cityData;
        if(city["cap"]) {
            cityData = renderData["cities_sprites"]["sprites"]["towers"][0];
        } else {
            cityData = renderData["cities_sprites"]["sprites"]["city"];
        }
        selectionContext.fillRect(
            city["x"] - cityData[4],
            city["y"] - cityData[5],
            cityData[2], cityData[3]);
    }
    selectionContext.restore();
}

/**
 * Draw the map in the view canvas: 
 * copy at the right place in view the pre-rendered chunks (background) that are in offscreen canvases,
 * and add animated entities (generals //TODO)
 */
function draw() {
    
    let firstColumn =  Math.floor((currentScroll.x - view.width * zoom / 2) / (chunkSize * zoom));
    let firstLine = Math.floor((currentScroll.y - view.height * zoom / 2) / (chunkSize * zoom));

    for(let i=0 ; i<nbChunksViewX ; i++) {
        for(let j=0 ; j<nbChunksViewY ; j++) {
            viewContext.drawImage(
                offscreenCanvasArray[(firstColumn+i+nbChunksViewX)%nbChunksViewX][(firstLine+j+nbChunksViewY)%nbChunksViewY],
                (firstColumn+i) * chunkSize - currentScroll.x / zoom + view.width / 2,
                (firstLine+j) * chunkSize - currentScroll.y / zoom + view.height / 2,
                chunkSize, chunkSize);
        }       
    }

    if(!hideTooltip){
        if(selectedEntity.type == 'city') {
           tooltip.innerHTML = selectedEntity.htmlData;
           tooltip.style.left = selectedEntity.x + 10 + 'px';
           tooltip.style.top = selectedEntity.y + 10 + 'px';
           tooltip.style.display = 'block'; 
        } else {
            tooltip.style.display = 'none'; 
        }
    }

    /*var thisLoop = new Date();
    var fps = 1000 / (thisLoop - lastLoop);
    lastLoop = thisLoop;
    console.log(fps);*/
}


/**
 * When finger or mouse is hovering the map, read selection offscreen canvas to check if a selectable entity
 * has been hovered.
 * 
 * @param {int} x x position of mouse or finger in selection (or view) canvas
 * @param {int} y y position of mouse or finger in selection (or view) canvas
 */
function updateSelectedEntity(x, y) {
    selectedEntity.x = x;
    selectedEntity.y =y;
    var pixelData = selectionContext.getImageData(selectedEntity.x, selectedEntity.y, 1, 1).data; 
    
    if(pixelData[2] > 0) {
        selectedEntity.type = "city";
        let oldId = selectedEntity.id;
        selectedEntity.id = pixelData[0] + pixelData[1] * 256 + (pixelData[2]-1) * 65536;

        if(selectedEntity.id !== oldId) {
            selectedEntity.htmlData = "<h2>" + mapGraph[selectedEntity.id]['name'] + "</h2>";
            if(mapGraph[selectedEntity.id]['capital']) {
                selectedEntity.htmlData += "<div>" + "Ville barbare" + "</div>"
            } else {
                selectedEntity.htmlData += "<div>" + "Lieu sauvage" + "</div>"
            }
        }
    } else {
        if(selectedEntity.type != "") {
            selectedEntity.type = "";
            selectedEntity.id = -1;
        }   
    }
}

/**
 * Recompute scrollMin and scrollMax depending on zoom value,
 * and adapt scroll position to fit in [scrollMin, scrollMax] if possible
 */
function updateScrollWithZoom() {

    scrollMin.x = view.width * zoom / 2 + 20;
    scrollMin.y = view.height * zoom /2 + 20;

    scrollMax.x = renderData['map_width'] - scrollMin.x;
    scrollMax.y = renderData['map_height'] - scrollMin.y;

    if(scrollMin.x > scrollMax.x) {
        scrollMin.x = (scrollMin.x + scrollMax.x) / 2;
        scrollMax.x = scrollMin.x;
    }
    if(scrollMin.y > scrollMax.y) {
        scrollMin.y = (scrollMin.y + scrollMax.y) / 2;
        scrollMax.y = scrollMin.y;
    }

    if(currentScroll.x < scrollMin.x) {
        currentScroll.x = scrollMin.x;
    }
    if(currentScroll.y < scrollMin.y) {
        currentScroll.y = scrollMin.y;
    }
    if(currentScroll.x > scrollMax.x) {
        currentScroll.x = scrollMax.x;
    }
    if(currentScroll.y > scrollMax.y) {
        currentScroll.y = scrollMax.y;
    }
}

function onMouseDragStart(event) {
    lastClientX = event.clientX;
    lastClientY = event.clientY;
    onDragStart(event);
}

function onTouchDragStart(event) {
    isPageScrollable = false;
    lastClientX = event.touches[0].clientX;
    lastClientY = event.touches[0].clientY;
    onDragStart(event);  
}

function onDragStart(event){
    hideTooltip = true;
    hasDragged = false;
    tooltip.style.display = 'none'; 
    document.addEventListener("mouseup", onDragEnd);
    document.addEventListener("touchend", onTouchEnd);
    document.addEventListener("mousemove", onMouseDragging);
    document.addEventListener("touchmove", onTouchDragging);
    view.removeEventListener('mousemove', onMouseMove);
}  

function onMouseDragging(event) {
    var deltaX = event.clientX - lastClientX;
    var deltaY = event.clientY - lastClientY;

    onDragging(deltaX, deltaY);

    lastClientX = event.clientX;
    lastClientY = event.clientY;
}

function onTouchDragging(event) {
    var deltaX = event.touches[0].clientX - lastClientX;
    var deltaY = event.touches[0].clientY - lastClientY;

    onDragging(deltaX, deltaY);

    lastClientX = event.touches[0].clientX;
    lastClientY = event.touches[0].clientY;
}

function onDragging(deltaX, deltaY){
    hasDragged = true;

    var newScrollx = currentScroll.x - zoom * deltaX;
    if (newScrollx < scrollMin.x) {
        currentScroll.x = scrollMin.x;
    } else if (newScrollx > scrollMax.x) {
        currentScroll.x = scrollMax.x;
    } else {
        currentScroll.x = newScrollx;
    }
    var newScrolly = currentScroll.y - zoom * deltaY;
    if (newScrolly < scrollMin.y) {
        currentScroll.y = scrollMin.y;
    } else if (newScrolly > scrollMax.y) {
        currentScroll.y = scrollMax.y;
    } else {
        currentScroll.y = newScrolly;
    }

    updateBackground(false);
    draw();
}

function onTouchEnd(event){
    isPageScrollable = true;
    onDragEnd();
}

function onDragEnd(){
    hideTooltip = false;
    document.removeEventListener("mousemove", onMouseDragging);
    document.removeEventListener("touchmove", onTouchDragging);
    document.removeEventListener("mouseup", onDragEnd);
    document.removeEventListener("touchend", onTouchEnd);
    view.addEventListener('mousemove', onMouseMove);
}

/**
 * When mouse is moving but not dragging (every button is up)
 */
function onMouseMove(event) {
    var rect = view.getBoundingClientRect();
    updateSelectedEntity(event.clientX - rect.left, event.clientY - rect.top);

    if(!hideTooltip){
        if(selectedEntity.type == 'city') {
           tooltip.innerHTML = selectedEntity.htmlData;
           tooltip.style.left = selectedEntity.x + 10 + 'px';
           tooltip.style.top = selectedEntity.y + 10 + 'px';
           tooltip.style.display = 'block'; 
        } else {
            tooltip.style.display = 'none'; 
        }
    }
}

/**
 * Hide tooltip when mouse is out of the map area
 */
function onMouseLeave(event) {
    hideTooltip = true;
    tooltip.style.display = 'none'; 
}

/**
 * Enable tooltip drawing when mouse is hovering the map area
 */
function onMouseOver(event) {
    hideTooltip = false;
}

function onClickMap(event) {
    if(!hasDragged) {
        var rect = view.getBoundingClientRect();
        updateSelectedEntity(event.clientX - rect.left, event.clientY - rect.top);

        if(selectedEntity.id !== -1) {
            openMapSidebar();
        } else {
            closeMapSidebar();
        }
    }
}

function onClickZoomOut(event) {
    zoom *= 2;
    if(zoom > zoomMax) {
        zoom = zoomMax;
    }
    updateScrollWithZoom();
    updateBackground(true);
    draw();
}

function onClickZoomIn(event) {
    zoom /= 2;
    if(zoom < 1) {
        zoom = 1;
    }
    updateScrollWithZoom();
    updateBackground(true);
    draw();
}

function onToggleStratMode() {
    isStrategicView = !isStrategicView;
    updateBackground(true);
    draw();
}

function openMapSidebar() {
    if( selectedEntity.type === "city") {
        document.getElementById("map_sidebar").style.width = "285px";
        document.getElementById("map_sidebar").style.left = "560px";
        document.getElementById("map_sidebar_content").innerHTML = selectedEntity.htmlData;
    }
}
  
function closeMapSidebar() {
    document.getElementById("map_sidebar").style.width = "0";
    document.getElementById("map_sidebar").style.left = "845px";
}


  
