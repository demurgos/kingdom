tabHeader: "City"

header:
  population: "Población"
  units: "Unidades"
  actions: "Acciones"
  buildings: "Edificios"
  logs: "Historial de tu capital"
  build: "Construir"
  soldierOptions: "Opciones de los soldados"
  convertResources: "Convertir recursos"
  recruitGeneral: "Reclutar un general"

buildings:
  palace:
    name: "Palacio"
    desc: "Es el edificio central de una capital, desde allí el reino es gobernado."
    upgradeDesc: "Mejorar tu palacio te permitirá mejorar los edificios en el siguiente nivel. Requiere el título de Señor."
  attic:
    name: "Granero"
    desc: "Es en el granero donde guardas los alimentos que te permitirán sobrevivir en caso de hambrunas prolongadas"
    upgradeDesc: "Mientras más grande sea el granero de la ciudad, mayores serán las reservas de <em>alimentos</em> que almacenarás."
  farm:
    name: "Granja"
    desc: "La granja es habitada por los campesinos y contribuye a alimentara toda la ciudad, ¡si produce el trigo suficiente!"
    upgradeDesc: "Mientras más se desarrolle la granja, mayor será la producción de los <em>campesinos</em>."
  hut:
    name: "Cabaña"
    desc: "La cabaña es el lugar de trabajo de tus Leñadores. Es un lugar ruidoso donde se guardan enormes cantidades de madera y al parecer tambien… mucho alcohol."
    upgradeDesc: "Una cabaña grande te permitirá aumentar la producción de los <em>Leñadores</em> y conservar reservas de madera para la ciudad."
  market:
    name: "Mercado"
    desc: "El mercado aumenta la actividad de los mercaderes en tu ciudad, lo cual te permite financiar tus costosas campañas militares."
    upgradeDesc: "Mejorando tu mercado, los <em>Mercaderes</em> tendrán más ganancias y producirán más oro."
  workshop:
    name: "Fabrica"
    desc: "Allí se fabrican las armas de guerra Balistas y Catapultas, particularmente mortiferas incluso con un minimo entrenamiento."
    upgradeDesc: "Una fábrica más grande reducirá los costos de elaboración de las <em>Balistas</em> y <em>Catapultas</em>, aumentando la productividad de tus <em>Obreros</em>."
  constructionSite:
    name: "Taller"
    desc: "Cuando los <em>obreros</em> no tienen nada que construir, los talleres los permiten mantenerse ocupados produciendo algo de oro y de alimentos. Pero hay que llevar siempre el casco puesto… ¡suelen divertirse lanzando martillos!"
    upgradeDesc: "Las mejoras en los talleres harán que tus <em>Obreros</em> produzcan un poco de alimentos y oro cuando efectúes la acción construir y que ningún edificio esté en construcción."
  barracks:
    name: "Cuartel"
    desc: "¡A enrolarse se ha dicho! El cuartel te permite reclutar Soldados que defenderán tu ciudad contra los ataques de tus enemigos."
    upgradeDesc: "Cada ampliación de tu cuartel te permitirá tener soldados que no requieren ningún salario."
  headquarters:
    name: "Cuartel general"
    desc: "El cuartel general es conocido por los deliciosos olores que escapan de él a la hora del almuerzo. Al menos eso dicen los Generales de paso que se reúnen allí."
    upgradeDesc: "Cada mejora en el cuartel general te permite controlar a un <em>General</em> adicional para reforzar tu ejército."
  wall:
    name: "Muralla"
    desc: "La muralla impide a los generales de otros jugadores penetrar en tu reino."
    upgradeDesc: "Mejorando tu muralla detendrás los ataques enemigos a tu capital, y reducirás también el número de duelos en una batalla."
  militaryAcademy:
    name: "Academia Militar"
    desc: "En la academia son formadas las unidades de élite, así como los hijos de los generales que sueñan con convertirse en Caballeros o Arqueros montados"
    upgradeDesc: "Mejorar la academia te permite reducir los costos de instrucción para que un soldado ascienda a <em>Caballero</em> o <em>Arquero Montado</em>."
  guardTower:
    name: "Torre de guardian"
    desc: "La torre de guardian permite convertir a simples soldados en Paladines. Desde arriba se puede divisar a los enemigos. Este también es uno de los lugares preferidos de kis desertores."
    upgradeDesc: "Mejorar la torre de guardia reduce el costo de ascender a un soldado al puesto de <em>Paladín</em>."
  stable:
    name: "Establos"
    desc: "El olfato te dirá donde encontrar un establo. Cuidado con criticar, los caballeros son muy susceptibles si se habla de sus potros."
    upgradeDesc: "Agrandando el establo reducirás el costo de ascender un soldado a <em>Caballero</em> y aumentarás tu capacidad de almacenar <em>caballos</em>."
  butcher:
    name: "Carniceria"
    desc: "El hambre a veces justifica los medios. En caso de hambruna, un <em>caballo</em> podrá siempre alimentar a tu población."
    upgradeDesc: "Mejorando el funcionamiento de la carnicería obtendrás más alimento por cada <em>caballo</em> (¡ñam!)"
  archery:
    name: "Arquería"
    desc: "Es mejor no pasearse muy cerca de una arquería a la hora del aperitivo, pues corres el riesgo de toparte con una fecha perdida por un arquero pasado de copas."
    upgradeDesc: "Ampliar la arquería reducirá tus costos al ascender un soldado al rango de <em>Arquero</em>."
  factory:
    name: "Fábrica textil"
    desc: "En la fábrica textil se elaboran vestimentas de <em>lino</em> que luego serán vendidas a cambio de oro a tus vecinos. Ese oro será usado posteriormente para invadirlos. Allí también se almacena <em>lino</em>."
    upgradeDesc: "Agrandar la fábrica textil aumentará tu capacidad de almacenar fibras de lino y de obtener más oro con la venta de prendas."
  forge:
    name: "Herrería"
    desc: "La Herrería es un lugarmuy caliente y lleno de olores nauseabundos. Allí se fabrican las lanzas que llevan los Infantes y se almacena metal."
    upgradeDesc: "Agrandar la herrería reduce el costo de ascenso de un soldado al rango de <em>Infante</em> y aumenta tu capacidad de almacenar <em>metal</em>."
  cauldron:
    name: "Caldero"
    desc: "Con el caldero podrás fundir enormes cantidades de tierra ocre para extraer un poco de <em>metal</em>. No es muy ecológico, pero funciona."
    upgradeDesc: "Mejorar el funcionamiento del caldero te permitirá tener un mejor rendimiento energético, con lo cual consumirás menos madera por unidad de <em>metal</em> producido."


units:
  soldier:
    name: "Soldado"
    desc: "Un soldado hará bien su trabajo mientras sea remunerado."
  pikeman:
    name: "Infante"
    desc: "Una larga lanza le da una ventaja de +150% contra las unidades a caballo."
  horseman:
    name: "Jinete"
    desc: "La rapidez de caballero le permite obtener un bonus de +100% contra los arqueros."
  archer:
    name: "Arquero"
    desc: "Gracias a sus flechas perforadoras, un arquero dispone de un bonus de +100% contra los infantes y paladines."
  paladin:
    name: "Paladin"
    desc: "Su armadura le da una resistencia que intimidará a sus adversarios."
  knight:
    name: "Caballero"
    desc: "Su rango de caballero le da una ventaja de +100% contra los soldados."
  mountedArcher:
    name: "Arquero Montado"
    desc: "Ágil, rápido, resistente y polivalente, el arquero montado anula todos los bonus de las unidades contra las que combate."
  catapult:
    name: "Catapulta"
    desc: "Esta unidad tiene un bonus de +200% pero sólo durante el ataque a una ciudad o a un territorio."
    buildDesc: "Construye una Catapulta para aumentar tu fuerza de ataque a las ciudades enemigas."
  ballista:
    name: "Balista"
    desc: "Esta unidad cuenta con un bonus de +200% pero sólo durante la defensa de una ciudad o de un territorio."
    buildDesc: "Construye una Balista para defender a tu cuidad de ataques enemigos."


resources:
  wheat:
    name: "Alimentos"
    desc: "Es producida por los campesinos y permite alimentar a la poblacion."
  gold:
    name: "Oro"
    desc: "Es producido por los mercaderes y permite pagar a los soldados."
  wood:
    name: "Madera"
    desc: "Es producida por les leñadores y es utilizada para construir edificios."
  iron:
    name: "Metal"
    desc: "Se extrae de las minas, y permite entre otras cosas, ascender a los soldados a paladines."
  lin:
    name: "Lino"
    desc: "Se encuentra en algunos lugares, y permite entre otras cosas, ascender a los soldados a arqueros."
  horse: 
    name: "Caballos"
    desc: "Los caballos son capturados en las praderas y permiten entre otras cosas ascender a los soldados a caballeros."
  

jobs:
  farmer:
    name: "Campesino"
    desc: "Produce los comestibles que permiten alimentar a la población."
  lumberjack:
    name: "Leñador"
    desc: "Provee madera a la cuidad permitiendo la construccion de edificios."
  worker:
    name: "Obrero"
    desc: "Trabaja construyendo los edificios de la cuidad."
  merchant:
    name: "Mercader"
    desc: "Produce oro gracias a sus comercios, lo cual permite pagar a los soldados"
  recruiter:
    name: "Reclutador"
    desc: "Facilita la formacion de los ejércitos."
  citizen:
    name: "Ciudadano"
    desc: "Los ciudadanos aún no han aprendido un oficio."


messages:
  remainingTurns: "Giros en espera"
  wastedTurns: "Giros desperdiciados"
  noAction: "Ninguna acción disponible por el momento."
  noArmy: "Ningún soldado en defensa actualmente."
  noWorker: "Recluta un obrero para construir tu palacio."
  soldierOptions:
    noRequiredBuilding: "Vous n\\'avez actuellement aucun bâtiment vous permettant d'améliorer vos soldats."
    unitPrice: "Precio por unidad"
    recruitment: 
      enable: "Activar el reclutamiento"
      disable: "Desactivar el reclutamiento"
      desc: "Esta acción va a modificar el reclutamiento de los soldados cuando se alineen las espadas."
  build:  
    start: "Construir"
    list: "Lista de edificios"
    cancel: "Anular la construcción"
    requireBuilding: "Requiere"
    requiresTitle: "Requiere el título de "
  pendingConstruction:
    message: "Construcción en marcha de"
    remaining: "necesarios"
  convert:
    spend: "Gastar"
    use: "Utiliser le"
    for: "para"
    quantity: "Cantidad"
  recruitGeneral:
    desc: "Un général vous permettra de déplacer vos unités et conquérir de nouveaux territoires pour agrandir votre royaume. Recruter ce général vous coutera "
  back : "Retornar"
  confirmAction: "¿Confirmas esta acción?"
  forbiddenSwap: "Puedes invertir 2 símbolos sólo si éstos te permiten alinear 3. El contorno blanco te indica tu selección."
  lvl: "Niv."
  level: "Nivel"