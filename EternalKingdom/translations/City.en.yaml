tabHeader: "City"

header:
  population: "Population"
  units: "Units"
  actions: "Actions"
  buildings: "Buildings"
  logs: "Your Capital's History"
  build: "Construct a Building"
  soldierOptions: "Options des soldats"
  convertResources: "Convertir des ressources"
  recruitGeneral: "Recruter un général"

buildings:
  palace:
    name: "Palace"
    desc: "Your Palace is the cornerstone of your Kingdom."
    upgradeDesc: "By improving your Palace, you\\'ll be able to upgrade all your other buildings."
  attic:
    name: "Granary"
    desc: "You can store food in the Granary, which is extremely useful in case there is a prolonged famine."
    upgradeDesc: "The bigger the Granary, the more <em>food</em> you can store."
  farm:
    name: "Farm"
    desc: "The Farm is where the Peasants live and work to help feed the population, provided that the wheat is growing well."
    upgradeDesc: "The more developed the Farm, the more food the <em>Peasants</em> produce."
  hut:
    name: "Cabin"
    desc: "The Cabin is where your <em>Lumberjacks</em> work. It\\'s piled full of wood, it\\'s also full of alcohol!"
    upgradeDesc: "A bigger Cabin will increase the wood production of your <em>Lumberjacks</em> and also the maximum amount of wood you can store."
  market:
    name: "Market"
    desc: "The Market increases the level of trade in your capital, this will allow you to finance costly military operations."
    upgradeDesc: "By upgrading your Market, you'll make your <em>Traders</em> more efficient and they'll produce more gold."
  workshop:
    name: "Workshop"
    desc: "The Workshop is where <em>Catapults</em> and <em>Ballistas</em> are built."
    upgradeDesc: "A bigger Workshop will lower the cost of building <em>Ballistas</em> or <em>Catapults</em> and will also increase your <em>Workers</em>\\' productivity."
  constructionSite:
    name: "Construction Site"
    desc: "When the <em>Workers</em> don\\'t have anything to build, they\\'ll produce small quantities of gold and food if there is an available Construction Site. Always make sure to wear a hard hat... especially when they start throwing hammers around!"
    upgradeDesc: "Upgrading the Construction Site allows your <em>Workers</em> to produce gold and food when you choose the \"build\" action and there are no buildings under construction."
  barracks:
    name: "Barracks"
    desc: "Sign up now! Barracks allow you to recruit <em>Soldiers</em> who will defend the village from enemy attacks."
    upgradeDesc: "Upgrading the Barracks allows you to have soldiers who will fight for you for no pay."
  headquarters:
    name: "General HQ"
    desc: "You can easily recognise the General HQ by the great smells that emanate from it around lunchtime. However, it\\'s not just a simple 3-Star cantina, it also provides shelter for <em>Generals</em> who are passing through."
    upgradeDesc: "Every time you upgrade the GHQ, you will be able to control an extra <em>General</em> to lead your armies."
  wall:
    name: "Wall"
    desc: "A wall can be used to prevent other players\\' Generals from travelling through your kingdom."
    upgradeDesc: "Upgrading the wall will slow down enemy attacks on your capital by reducing the number of duels during a battle."
  militaryAcademy:
    name: "Military Academy"
    desc: "The Academy is where elite troops are trained. Strings are regularly pulled so that sons of generals who dream of becoming <em>Knights</em> or <em>Mounted Archers</em> can study here."
    upgradeDesc: "By improving the Academy, you\\'ll reduce the cost of training soldiers to become <em>Knights</em> or <em>Mounted Archers</em>."
  guardTower:
    name: "Guard Tower"
    desc: "The Guard Tower is valuable strategic viewpoint which because of its height, can be used to see enemy forces from miles away. This is also where soldiers are trained to become <em>Paladins</em>."
    upgradeDesc: "Upgrading your Guard Tower will reduce the cost of training a soldier to become a <em>Paladin</em>."
  stable:
    name: "Stables"
    desc: "The Stables can be easily located - just follow your nose! Whatever you do don\\'t mention the smell to the <em>Knights</em>! They\\'re very touchy when it comes to their horses."
    upgradeDesc: "Upgrading the Stables reduces the cost of training a Soldier to become a <em>Knight</em>. It also increases the maximum stock of horses you can keep."
  butcher:
    name: "Butcher"
    desc: "The end often justifies the means. If there is a famine, you can sacrifice a horse to feed your population."
    upgradeDesc: "Upgrading the Butcher\\'s shop will produce more food from each horse! (Mmmm - tasty!)"
  archery:
    name: "Archery Range"
    desc: "It\\'s better not to walk past the Archery Range after ye olde taverne shuts, or you may end up like poor old King Harold, courtesy of a lagered-up <em>Archer</em> !"
    upgradeDesc: "Upgrading the Archery Range reduces the cost of training a soldier to become an <em>Archer</em>."
  factory:
    name: "Factory"
    desc: "The Factory is used to make clothes using <em>Cotton</em> which are then sold to your neighbours in exchange for gold, who you will then invade. The Factory is also used to store <em>Cotton</em>."
    upgradeDesc: "Upgrading the Factory increase the maximum stock of cotton you can keep."
  forge:
    name: "Forge"
    desc: "The forge is always overheated and full of nauseating fumes. <em>Jousters</em>\\' lances are made here and it is also used for storing metal."
    upgradeDesc: "Upgrading the forge reduces the cost of training soldiers to become <em>Jousters</em>. This also increases the maximum amount of <em>metal</em> you can store."
  cauldron:
    name: "Cauldron"
    desc: "The Cauldron is used to extract <em>metal</em> from its ore. It\\'s not very environmentally friendly, but it works nonetheless."
    upgradeDesc: "Upgrading the Cauldron improves energy efficiency using less wood for each unit of <em>metal</em> produced."


units:
  soldier:
    name: "Soldier"
    desc: "A good soldier who carries out his job as expected........as long as he\\'s paid on time."
  pikeman:
    name: "Pikeman"
    desc: "A pikeman\\'s long pike gives him a +150% advantage against mounted enemy troops."
  horseman:
    name: "Cavalier"
    desc: "The speed of a cavalier means that they have a bonus of +100% against archers."
  archer:
    name: "Archer"
    desc: "Thanks to his armour piercing arrows, an archer has a +100% bonus when used against cavaliers and knights."
  paladin:
    name: "Knight"
    desc: "A knight\\'s armour gives him heavy protection which discourages the majority of his opponents."
  knight:
    name: "Mounted Knight"
    desc: "A mounted knight\\'s power gives him an advantage of +100% against soldiers."
  mountedArcher:
    name: "Mounted Archer"
    desc: "Agile, rapid, strong and versatile, a mounted archer cancels all bonuses of enemy troops."
  catapult:
    name: "Catapult"
    desc: "A catapult has a bonus of +200%, but only when attacking a village or another location."
    buildDesc: "Construisez une <em>Catapulte</em> pour vous aider lors des attaques des villes adverses."
  ballista:
    name: "Ballista"
    desc: "A ballista has a bonus of +200%, but only when defending a village or another location."
    buildDesc: "Construisez une <em>Baliste</em> pour défendre la ville contre les attaques de vos adversaires."


resources:
  wheat:
    name: "Food"
    desc: "Food is produced by peasants and is used to feed the population."
  gold:
    name: "Gold"
    desc: "Gold is acquired by traders and is used to pay soldiers."
  wood:
    name: "Wood"
    desc: "Wood is collected by lumberjacks and is used to construct buildings."
  iron:
    name: "Metal"
    desc: "Metal is extracted from mines and can be used to make armour for soldiers."
  lin:
    name: "Cotton"
    desc: "Cotton can be found in certain locations and can be used to train soldiers to become archers."
  horse:
    name: "Horses"
    desc: "Horses are kept in meadows and can be used to train soldiers how to fight on horseback."


jobs:
  farmer:
    name: "Peasant"
    desc: "Peasants produce food to feed the population."
  lumberjack:
    name: "Lumberjack"
    desc: "Lumberjacks provide villages with wood to construct buildings."
  worker:
    name: "Worker"
    desc: "Workers help to construct buildings."
  merchant:
    name: "Trader"
    desc: "Traders produce gold which can be used to pay soldiers."
  recruiter:
    name: "Recruiter"
    desc: "Recruiters help to build an army."
  citizen:
    name: "Citizen"
    desc: "Citizens haven\\'t yet chosen a job."


messages:
  remainingTurns: "Tours en attente"
  wastedTurns: "Tours gaspillés "
  noAction: "Aucune action n'est disponible pour l'instant"
  noArmy: "Your capital is undefended"
  noWorker: "Recrutez un ouvrier pour construire votre palais."
  soldierOptions:
    noRequiredBuilding: "Vous n\\'avez actuellement aucun bâtiment vous permettant d'améliorer vos soldats."
    unitPrice: "Prix par unité :"
    recruitment: 
      enable: "Activer le recrutement"
      disable: "Desactiver le recrutement" 
      desc: "Cette action va modifier le recrutement des soldat lors de l\\'alignement d\\'épées."
  build:  
    start: "Build"
    list: "Liste des batiments"
    cancel: "Cancel the construction"
    requireBuilding: "Requires"
    requiresTitle: "Requires the title "
  pendingConstruction:
    message: "Construction en cours de"
    remaining: "restants"
  convert:
    spend: "Dépenser"
    use: "Utiliser le"
    for: "pour"
    quantity: "Quantité"
  recruitGeneral:
    desc: "Un général vous permettra de déplacer vos unités et conquérir de nouveaux territoires pour agrandir votre royaume. Recruter ce général vous coutera "
  back : "Back"
  confirmAction: "Confirm this action?"
  forbiddenSwap: "Vous ne pouvez inverser deux symboles que si ceux-ci vous permettent d en aligner trois. Une action est toujours possible et est indiquée par un léger contour blanc."
  lvl: "Lvl"
  level: "Level"